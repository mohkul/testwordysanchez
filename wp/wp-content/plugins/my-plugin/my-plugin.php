<?php
/*
* Plugin Name: My Plugin
* Plugin URI: http://myplugin.com/
* Description: This is my plugin description.
* Author: Erick Soto
* Version: 1.0.0
* Author URI: http://erickrsoto.com
* License: GPLv2 or later
*/


// Add admin dashboard

function wordy_admin_menu() {
	add_menu_page(
		'Wordy'
	);
}

add_action( 'admin_menu', 'wordy_admin_menu' );

function wordy_reports_page() {
	require_once dirname( __FILE__ ) . "wordy/reports.php";
}