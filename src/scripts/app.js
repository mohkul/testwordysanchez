
var dataProxy  = require('./dataProxy'),
	Router     = require('./router'),
	Controller = require('./controller');
	

var App = function() {
	this.core = new Marionette.Application();
}

App.prototype.start = function(options) {
	var that = this;
	dataProxy.fetch();
	dataProxy.on('dataReady', function() {
		that.core.start();
	});
}

App.prototype.dataReady = function() {

}

module.exports = app = new App();

app.core.addRegions({
	rootRegion: '.site'
});


app.core.addInitializer(function(options) {
	// init
});


app.core.on('start', function(options) {

	this.controller = new Controller(this);
	this.router = new Router({controller: this.controller});

	if (Backbone.history) {
		Backbone.history.start({root:'dist'});
	}

});