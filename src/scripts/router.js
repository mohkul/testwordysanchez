
module.exports = Router = Marionette.AppRouter.extend({
   
    appRoutes: {
        ""                   :   "home",
        "posts/:id"          :   "post",
        "posts/:id/:slug"    :   "post",
        "category/:id"       :   "category",
        "wordy/:id"          :   "wordy"
    }

});