
Layout = require('./views/mainLayoutView');

module.exports = Controller = Marionette.Controller.extend({
   
    initialize: function(app) {
        this.layout = new Layout();
        app.rootRegion.show( this.layout );
    },

    home: function() {
        this.layout.home();
    },

    post: function(id) {
        this.layout.post(id)
    },

    category: function(id) {
        this.layout.category(id);
    },

    wordy: function(id) {
        this.layout.wordies(id);  
    }

});