
var template = require('../templates/footer.html');

module.exports = FooterView = Marionette.ItemView.extend({
    template: template
});