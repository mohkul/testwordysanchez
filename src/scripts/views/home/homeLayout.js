
var template = require('../../templates/home/posts.html'),
    articleTpl = require('../../templates/home/article.html'),
    dataProxy = require('../../dataProxy');

var Article = Marionette.ItemView.extend({
    tagName: 'article',
    className: 'post item2',
    template: articleTpl
});

var Articles = Marionette.CollectionView.extend({
    className: 'row',
    childView: Article,
});

module.exports = Marionette.LayoutView.extend({
    template: template,

    postOffset: 12,
    loadThisMore : 6,

    moreCollection: new Backbone.Collection(),

    regions: {
        'moreRegion' : '.more-posts'
    },

    onShow: function() {
        var that = this;
        $('.main-nav a').removeClass('active');

        this.moreCollection = new Backbone.Collection();
        this.moreViews = new Articles({
            collection: this.moreCollection
        })

        this.moreRegion.show( this.moreViews );

        thispostOffset = 12;
        this.resetPosts();
    },

    onDestroy: function() {
        // dataProxy.getPosts().each(function(m, i) {
        //     m.set('loaded', false)
        // });
    },

    events: {
        'click .load-more' : 'loadMore'
    },

    loadMore: function() {
        
        var data = dataProxy.getPosts().where({'loaded':false}).slice(0, this.loadThisMore);

        if (_.isEmpty(data)) {
            return $('.load-more').fadeOut();
        }

        this.moreCollection.add(data);

        this.postOffset += this.loadThisMore;
        this.resetPosts();
    },

    resetPosts: function() {
        var that = this;
        dataProxy.getPosts().each(function(m, i) {
            if (i < that.postOffset) {
                m.set('loaded', true);
            }
            else {
                m.set('loaded', false);
            }
        });
    }
});