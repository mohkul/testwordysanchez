
var template = require('../templates/main-layout.html'),
    dataProxy = require('../dataProxy'),

    // Views
    TopnavView       = require('./topnavView'),
    SidenavView      = require('./sidenavView'),
    FooterView       = require('./footerView'),

    HomefeaturedView = require('./home/featuredView'),

    HomeLayout       = require('./home/homeLayout'),

    PostView         = require('./postView'),

    CategoryLayout   = require('./category/categoryLayout');


module.exports = MainLayout = Marionette.LayoutView.extend({

    template: template,

    views: {},

    regions: {
        topnavRegion      : '.topnav',
        sidenavRegion     : '.side-menu',
        footerRegion      : '.main-footer',

        featuredRegion    : '.home-content',
        mainContentRegion : '.main-content'
    },

    events: {
        'click .menu-icon, .menu-overlay' : 'toggleMenu'
    },

    onRender: function() {

        // topnav
        var menuModel = new Backbone.Model({
            'menu': dataProxy.getCategories({'main-menu':'checked'}).toJSON()
        });
        this.views.topnav = new TopnavView({model: menuModel});
        this.topnavRegion.show( this.views.topnav );

        // sidenav
        var sidenavModel = new Backbone.Model({
            'categories': dataProxy.getCategories().toJSON(),
            'wordies': dataProxy.getWordies().toJSON()
        });
        this.views.sidenav = new SidenavView({model: sidenavModel});
        this.sidenavRegion.show( this.views.sidenav );

        this.views.footer = new FooterView();
        this.footerRegion.show( this.views.footer );

    },


    home: function() {

        // featured
        var featured = dataProxy.getPosts().filter(function(model) {
            var f = model.get('featured_image');
            return ((typeof(f) !== 'undefined') && (f!==null));
        });

        var featuredModel = new Backbone.Model({
            'categories': dataProxy.getCategories().toJSON(),
            'featured' : featured.slice(0, 3)
        });
        this.views.homefeaturedView = new HomefeaturedView({model: featuredModel});
        this.featuredRegion.show( this.views.homefeaturedView );


        // home
        this.views.homeLayout = new HomeLayout({
            collection: dataProxy.getPosts()
        });
        this.mainContentRegion.show( this.views.homeLayout );
    },


    post: function(id) {
        var that = this;
        if (this.views.homefeaturedView) {
            this.views.homefeaturedView.destroy();
        }
        
        dataProxy.getPostById(id, function(m) {
            var model = m;
            var category_id = model.get('terms').category[0].slug;
            var related = dataProxy.getPostsByCategory(category_id).toJSON();
            model.set('related', related);
            
            that.views.post = new PostView({
                model: model
            });
            that.mainContentRegion.show( that.views.post );
        });
    },


    category: function(id) {
        var that = this;
        if (this.views.homefeaturedView) {
            this.views.homefeaturedView.destroy();
        }

        var model = dataProxy.getCategories().findWhere({'slug': id});
        model.set('items', dataProxy.getPostsByCategory(id).toJSON());
        model.set('wordies', dataProxy.getWordies().toJSON());

        // category
        this.views.categoryLayout = new CategoryLayout({
            model: model
        });
        this.mainContentRegion.show( this.views.categoryLayout );
    },


    wordies: function(id) {
        var that = this;
        if (this.views.homefeaturedView) {
            this.views.homefeaturedView.destroy();
        }

        var model = dataProxy.getWordies().findWhere({'slug': id});
        model.set('items', dataProxy.getPostsByWordies(id).toJSON());
        model.set('wordies', dataProxy.getWordies().toJSON());

        // category
        this.views.categoryLayout = new CategoryLayout({
            model: model
        });
        this.mainContentRegion.show( this.views.categoryLayout );
    },


    toggleMenu: function(e) {
        if ($('.menu-anim').hasClass('animate'))
        {
            $('.menu-anim').removeClass('animate');
            $('body').removeClass('menu-on');
        }
        else {
            $('.menu-anim').addClass('animate');
            $('body').addClass('menu-on');
        }
    }


});