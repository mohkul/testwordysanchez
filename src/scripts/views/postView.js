
var template = require('../templates/post.html');

module.exports = PostView = Marionette.ItemView.extend({
    template: template,

    onShow: function() {
        $(window).scrollTop(0);
    }

});