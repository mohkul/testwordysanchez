
var template = require('../templates/sidenav.html');

module.exports = SidenavView = Marionette.ItemView.extend({
    template: template,

    events: {
        'click li a' : 'closeMenu'
    },

    closeMenu: function() {
        $('.menu-icon').click();
    }
});