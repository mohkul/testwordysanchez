
var template = require('../../templates/category/layout.html');

module.exports = Marionette.LayoutView.extend({
    template: template,

    onShow: function() {
        $(window).on('scroll', this.onScroll);
        this.onScroll();

        $(window).on('resize', this.onResize);
        this.onResize();

        $(window).scrollTop(0);
    },

    onScroll: function(e) {
        var _t = $(window).scrollTop();

        if (_t >= 55) {
            $('.right-side').addClass('fixed');
        }
        else {
            $('.right-side').removeClass('fixed');
        }
    },

    onResize: function(e) {
        var $mainW = $('.main-block').width(),
            $windowW = $(window).width(),
            percent = 38,
            width = $mainW / 100 * percent;

        $('.right-side').css({
            'right' : ($windowW - $mainW) / 2,
            'width' : $mainW / 100 * percent
        });
    }
});