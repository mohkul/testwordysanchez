
var template = require('../templates/topnav.html');

module.exports = TopnavView = Marionette.ItemView.extend({
    template: template,

    events: {
        'click .text-logo' : 'goHome',
        'click .main-nav a' : 'gotoCategory'
    },

    goHome: function(e) {
        e.preventDefault();
        Backbone.history.navigate('', true);
    },


    gotoCategory: function(e) {
        e.preventDefault();
        var $target = $(e.currentTarget);
        var href = $target.attr('href');
        Backbone.history.navigate('category/'+href, true);

        $('.main-nav a').removeClass('active');
        $target.addClass('active');
    }
});