
var Posts = Backbone.Collection.extend({
    
    url: function() {
        // return '/api/posts';
        return '/wp/wp-json/posts';
    },

    category_filter: function(category_id) {
        var filtered = _.filter(this.toJSON(), function(m) {
            var filter = false
            _.each(m.terms.category, function(c) {
                if (c.slug === category_id) {
                    filter = true;
                }
            })

            return filter;
        });

        return new Backbone.Collection( filtered );
    },

    wordy_filter: function(wordy_id) {
        var filtered = _.filter(this.toJSON(), function(m) {
            var filter = false
            _.each(m.terms.wordy, function(c) {
                if (c.slug === wordy_id) {
                    filter = true;
                }
            })

            return filter;
        });

        return new Backbone.Collection( filtered );
    }

});

module.exports = Posts;