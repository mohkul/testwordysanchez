
var MenuModel = Backbone.Model;

var MenuCollection = Backbone.Collection.extend({
    
    model: MenuModel,

    url: function() {
        return '/wp/wp-json/taxonomies/menu/terms';
    }

});

module.exports = MenuCollection;