
var CategoryModel = Backbone.Model;

var CategoryCollection = Backbone.Collection.extend({
    
    model: CategoryModel,

    url: function() {
        return '/api/term/category';
    },
    
    parse: function(data) {
        var filtered = [];
        _.each(data, function(i) {
            if (i.count > 0) {
                filtered.push(i)
            }
        });
        
        return filtered;
    }

});

module.exports = CategoryCollection;