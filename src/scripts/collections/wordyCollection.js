
var WordyModel = Backbone.Model;

var WordyCollection = Backbone.Collection.extend({
    
    model: WordyModel,

    url: function() {
        return '/api/term/wordy';
    },

    parse: function(data) {
        var filtered = [];
        _.each(data, function(i) {
            if (i.count > 0) {
                filtered.push(i)
            }
        });
        
        return filtered;
    }

});

module.exports = WordyCollection;