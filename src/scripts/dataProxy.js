
var PostModel             = require('./model/postModel'),
    PostsCollection       = require('./collections/postsCollection'),
    CategoryCollection    = require('./collections/categoryCollection'),
    WordyCollection       = require('./collections/wordyCollection'),

    postModel             = new PostModel(),
    postsCollection       = new PostsCollection(),
    categoryCollection    = new CategoryCollection(),
    wordyCollection       = new WordyCollection();


module.exports = _.extend({

    categoriesReady: false,
    wordiesReady: false,
    postsReady: false,

    isReady: function() {
        if (this.categoriesReady && this.wordiesReady && this.postsReady) {
            this.trigger('dataReady');
        }
    },

    fetch: function() {
        var that = this;
        categoryCollection.fetch({
            success: function() {
                that.categoriesReady = true;
                that.isReady();
            }
        });

        wordyCollection.fetch({
            success: function() {
                that.wordiesReady = true;
                that.isReady();
            }
        });

        postsCollection.fetch({
            success: function() {
                that.postsReady = true;
                that.isReady();
            }
        });
    },

    getCategories: function(where) {
        if (where) {
            return new Backbone.Collection(categoryCollection.where(where));
        }
        else {
            return categoryCollection;
        }
    },

    getWordies: function() {
        return wordyCollection;
    },

    getPosts: function() {
        window.postsCollection = postsCollection;
        return postsCollection;
    },

    getPostsByCategory: function(category_id) {
        return postsCollection.category_filter(category_id);
    },


    getPostsByWordies: function(category_id) {
        return postsCollection.wordy_filter(category_id);
    },

    getPostById: function(id, callback) {
        postModel.fetch({
            url: '/wp/wp-json/posts/'+id,
            success: function(m) {
                callback(m)
            }
        })
    }


}, Backbone.Events);