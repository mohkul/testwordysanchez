<?php
	include 'includes/data.php';
	$section = (isset($_GET['s'])) ? $_GET['s'] : null;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Wordy Sanchez</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes"> 

	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/main.css">
	
	<script type="text/javascript" src="//use.typekit.net/wib1dci.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body class="section-<?php echo $sections[$section]['slug']?>">

	<div class="site">

		<?php include 'includes/topnav.php'; ?>

		<div class="page-heading <?php echo $sections[$section]['slug']?>">
			<div class="inner">
				<h2><?php echo $sections[$section]['name']?></h2>
			</div>
		</div>

		<div class="main-content">

			<div class="posts">
				
				<div class="main-block">

					<aside class="right-side">

							
						<div class="trending-stories wordies <?php echo $sections[$section]['slug']?>">
							<div class="topic <?php echo $sections[$section]['slug']?>"><?php echo $sections[$section]['name']?></div>
							<h3 class="section-heading">WORDIES</h3>
							<div class="links">
								<div class="link">
									<a href="#">All ( 6 )</a>
								</div>
								<div class="link">
									<a href="#">You May Be Cousins ( 4 )</a>
								</div>
								<div class="link">
									<a href="#">Crate Digging ( 2 )</a>
								</div>
							</div>
						</div>


						<div class="ad2"></div>

					</aside>

	

					<?php foreach ($posts['row2'] as $row) { ?>
					<article class="post item1">
							
						<div class="media">
							<div class="category"><a href="<?php echo $sections[$section]['slug']?>" class="<?php echo $sections[$section]['slug']?>"><?php echo $sections[$section]['name']?></a></div>
							<a href="/story/<?php echo $row->id?>"><img src="<?php echo $row->image?>" alt="" width="100%"></a>
						</div>

						<div class="meta">
							<a href="/story/<?php echo $row->id?>" class="title"><h1><?php echo $row->title?></h1></a>
							<div class="author-date">
								<span class="author">By <a href="#">Stan Lee</a></span>
								<span class="date">on 08-15-2014 4:56pm</span>
							</div>
							<div class="excerpt">
  								<p><?php echo $row->excerpt?></p>
							</div>
						</div>
					</article>
					<?php } ?>


				</div>









			</div>

		</div>


		<div class="main-footer">
			<div class="inner">
				<span>FOOTER</span>
			</div>
		</div>

	</div>


	<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
	<script>

		var Inner = function() {
			$(window).on('scroll', this.onScroll);
			this.onScroll(null);
		};

		Inner.prototype.onScroll = function(e) {
			var _t = $(window).scrollTop();

			if (_t >= 55) {
				$('.right-side').addClass('fixed');
			}
			else {
				$('.right-side').removeClass('fixed');
			}
		};

		new Inner();

	</script>
	<script src="/js/main.js"></script>

</body>

</html>