<?php
	$section = 'story';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Wordy Sanchez</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes"> 

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/main.css">

	<script type="text/javascript" src="//use.typekit.net/wib1dci.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body>

	<div class="site">

		<?php include 'includes/topnav.php'; ?>


		<div class="featured menu-anim">
			
			<div class="hero-section">

				<div class="item item1" style="background-image:url(http://cdn3.vox-cdn.com/uploads/chorus_image/image/37442106/twitch_logo_purple.0_cinema_640.0.jpg)">

					<div class="content">
						<h1>Lorem ipsum dolor sit amet, ea per mundi deterruisset</h1>
					</div>

				</div>

				<div class="item item2" style="background-image:url(http://cdn3.vox-cdn.com/uploads/chorus_image/image/37423736/news_photo_40503_1400594699.0.0_cinema_384.0.jpg)">

					<div class="content">
						<h1>Lorem ipsum dolor sit amet, ea per mundi deterruisset</h1>
					</div>

				</div>

				<div class="item item3" style="background-image:url(http://cdn2.vox-cdn.com/uploads/chorus_image/image/37444714/halo_2.0_cinema_384.0.jpg)">

					<div class="content">
						<h1>Lorem ipsum dolor sit amet, ea per mundi deterruisset</h1>
					</div>

				</div>

			</div>


		</div>

		<div class="main-nav nav2 menu-anim">
			<nav>
			<ul>
				<li><a href="comics" class="comics">Comics</a></li>
				<li><a href="films" class="films">Films</a></li>
				<li><a href="tv" class="tv">TV</a></li>
				<li><a href="video-games" class="video-games">Video Games</a></li>
				<li><a href="cosplay" class="cosplay">Cosplay</a></li>
				<li><a href="anime" class="anime">Anime</a></li>
				<li><a href="gadgets" class="gadgets">Gadgets</a></li>
				<li><a href="screenings" class="screenings">Screenings</a></li>
				<li><a href="features" class="features">Features</a></li>
			</ul>
			</nav>
		</div>

		<div class="ad"></div>

		<div class="main-content">

			<div class="posts">
				
				<div class="main-block">

					<article class="post item1">
							
						<div class="media">
							<div class="category"><a href="comics" class="comics">COMICS</a></div>
							<img src="images/uploads/2014/08/MiddleearthShadowofMordor_Gollum_Screenshot_epic.jpg" alt="" width="100%">
						</div>

						<div class="meta">
							<h1>Middle-earth: Shadow of Mordor screenshots show Gollum in action</h1>
							<div class="author-date">
								<span class="author">By <a href="#">Stan Lee</a></span>
								<span class="date">on 08-15-2014 4:56pm</span>
							</div>
							<div class="excerpt">
  								<p>The latest screenshots for Monolith Productions’ Middle-earth: Shadow of Mordor, fresh out of Gamescom, show Gollum in action, a strange and lowly creature familiar to fans ofThe Hobbit and The Lord of the Rings. In J.R.R. Tolkien’s literature, Gollum was once a Hobbit named Sméagol who found the One Ring of Power. Over countless dozens of years the</p>
							</div>
						</div>
					</article>

					<div class="trending-stories">
						<h3 class="section-heading">TRENDING STORIES</h3>
						<div class="links">
							<div class="link">
								<a href="#">Lorem ipsum dolor sit amet, ea per mundi deterruisset...</a>
							</div>
							<div class="link">
								<a href="#">Lorem ipsum dolor sit amet, ea per mundi deterruisset...</a>
							</div>
							<div class="link">
								<a href="#">Lorem ipsum dolor sit amet, ea per mundi deterruisset...</a>
							</div>
							<div class="link">
								<a href="#">Lorem ipsum dolor sit amet, ea per mundi deterruisset...</a>
							</div>
							<div class="link">
								<a href="#">Lorem ipsum dolor sit amet, ea per mundi deterruisset...</a>
							</div>
							<div class="link">
								<a href="#">Lorem ipsum dolor sit amet, ea per mundi deterruisset...</a>
							</div>
							<div class="link">
								<a href="#">Lorem ipsum dolor sit amet, ea per mundi deterruisset...</a>
							</div>
							<div class="link">
								<a href="#">Lorem ipsum dolor sit amet, ea per mundi deterruisset...</a>
							</div>
						</div>
					</div>


					<div class="ad2"></div>


				</div>







			</div>

		</div>


		<div class="main-footer">
			<div class="inner">
				<span>FOOTER</span>
			</div>
		</div>

	</div>


	<script type="text/template" id="navigation">
		<nav>
			<ul>
				<% _.each(categories, function(i) { %>
					<% if (i.slug!=='uncategorized') { %>
					<li><a href="<%=i.slug%>" class="<%=i.slug%>"><%=i.name%></a></li>
					<% } %>
				<% }); %>
			</ul>
		</nav>
	</script>
	

	<script type="text/template" id="wordies">
		<h2>WORDIES</h2>
		<ul>
			<% _.each(wordies, function(i) { %>
				<li><a href="#"><%=i.name%></a></li>
			<% }); %>
		</ul>
	</script>



	<script type="text/template" id="post">
		<article class="post item1">
			
			<div class="category"><a href="#"><%=terms.category[0].name%></a></div>

			<% if (featured_image) { %>
				<div class="media">
					<img src="<%=featured_image.source%>" alt="" width="100%">
				</div>
			<% } %>

			<div class="meta">
				<h1><%=title%></h1>
				<div class="author-date">
					<span class="author">By <a href="#"><%=author.first_name%> <%=author.last_name%></a>
					</span>
					<span class="date">on <%=date%></span>
				</div>
				<%=excerpt%>
			</div>
		</article>
	</script>



	<script type="text/template" id="post1">
		<article class="post item1">
			<div id="kaltura_player_1408579777" style="width: 400px; height: 333px;" itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
			<span itemprop="name" content="Kaltura Logo Image"></span>
			<span itemprop="description" content="Kaltura logo"></span>
			<span itemprop="duration" content="0"></span>
			<span itemprop="thumbnail" content="http://cdnbakmi.kaltura.com/p/1788471/sp/178847100/thumbnail/entry_id/1_b2jt93hx/version/100000/acv/11"></span>
			<span itemprop="width" content="400"></span>
			<span itemprop="height" content="333"></span>
			<a href="http://corp.kaltura.com/products/video-platform-features">Video Platform</a>
			<a href="http://corp.kaltura.com/Products/Features/Video-Management">Video Management</a> 
			<a href="http://corp.kaltura.com/Video-Solutions">Video Solutions</a>
			<a href="http://corp.kaltura.com/Products/Features/Video-Player">Video Player</a></div>
			<script src="http://cdnapi.kaltura.com/p/1788471/sp/178847100/embedIframeJs/uiconf_id/25101282/partner_id/1788471?autoembed=true&entry_id=1_b2jt93hx&playerId=kaltura_player_1408579777&cache_st=1408579777&width=400&height=333"></script>
		</article>
	</script>



	<script type="text/template" id="post2">
		<article class="post item2">
			
			<div class="category"><a href="#"><%=terms.category[0].name%></a></div>

			<% if (featured_image) { %>
				<div class="media">
					<img src="<%=featured_image.source%>" alt="" width="100%">
				</div>
			<% } %>

			<div class="meta">
				<h1><%=title%></h1>
				<div class="author-date">
					<span class="author">By <a href="#"><%=author.first_name%> <%=author.last_name%></a>
					</span>
					<span class="date">on <%=date%></span>
				</div>
				<%=excerpt%>
			</div>
		</article>
	</script>


	<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
	<script>

		var Story = function() {
		};

		new Story();

	</script>
	<script src="js/main.js"></script>

</body>

</html>