<?php
	$page_id = $_GET['id'];
	include 'includes/data.php';
	$section = 'story';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Wordy Sanchez</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes"> 

	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/main.css">
	
	<script type="text/javascript" src="//use.typekit.net/wib1dci.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body class="story">

	<div class="site">

		<?php include 'includes/topnav.php'; ?>

		<div class="page-heading">
			<div class="inner">
				<h2>STORY</h2>
			</div>
		</div>

		<div class="main-content">

			<div class="posts">
				
				<div class="main-block">

					<article class="post item1">
						
						<h1><?php echo $post->title?></h1>

						<div class="meta">
							
							<div class="author-date">
								<span class="author">By <a href="#">Stan Lee</a></span>
								<span class="date">on 08-15-2014 4:56pm</span>
							</div>
							<div class="content">
  								<?php echo $post->content?>
							</div>
						</div>
					</article>


					<div class="trending-stories">
						<h3 class="section-heading">RELATED STORIES</h3>
						<div class="links">
							<div class="link">
								<a href="#">Lorem ipsum dolor sit amet, ea per mundi deterruisset...</a>
							</div>
							<div class="link">
								<a href="#">Lorem ipsum dolor sit amet, ea per mundi deterruisset...</a>
							</div>
							<div class="link">
								<a href="#">Lorem ipsum dolor sit amet, ea per mundi deterruisset...</a>
							</div>
							<div class="link">
								<a href="#">Lorem ipsum dolor sit amet, ea per mundi deterruisset...</a>
							</div>
							<div class="link">
								<a href="#">Lorem ipsum dolor sit amet, ea per mundi deterruisset...</a>
							</div>
							<div class="link">
								<a href="#">Lorem ipsum dolor sit amet, ea per mundi deterruisset...</a>
							</div>
							<div class="link">
								<a href="#">Lorem ipsum dolor sit amet, ea per mundi deterruisset...</a>
							</div>
							<div class="link">
								<a href="#">Lorem ipsum dolor sit amet, ea per mundi deterruisset...</a>
							</div>
						</div>
					</div>

				</div>


			</div>

		</div>


		<div class="main-footer">
			<div class="inner">
				<span>FOOTER</span>
			</div>
		</div>

	</div>


	<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
	<script>

		var Story = function() {

		};

		new Story();

	</script>
	<script src="/js/main.js"></script>

</body>

</html>