
$(function() {
	
	$('.menu-icon, .menu-overlay').on('click', function(e) {
		if ($('.menu-anim').hasClass('animate'))
		{
			$('.menu-anim').removeClass('animate');
			$('body').removeClass('menu-on');
		}
		else {
			$('.menu-anim').addClass('animate');
			$('body').addClass('menu-on');
		}
	});



	var App = function() {
		$(window).on('resize', this.onResize);
		this.onResize();
	};

	App.prototype.onResize = function(e) {
		var $mainW = $('.main-block').width(),
			$windowW = $(window).width(),
			percent = 38,
			width = $mainW / 100 * percent;

		$('.right-side').css({
			'right' : ($windowW - $mainW) / 2,
			'width' : $mainW / 100 * percent
		});
	};

	new App();





});