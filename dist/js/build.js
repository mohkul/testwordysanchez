(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

var dataProxy  = require('./dataProxy'),
	Router     = require('./router'),
	Controller = require('./controller');
	

var App = function() {
	this.core = new Marionette.Application();
}

App.prototype.start = function(options) {
	var that = this;
	dataProxy.fetch();
	dataProxy.on('dataReady', function() {
		that.core.start();
	});
}

App.prototype.dataReady = function() {

}

module.exports = app = new App();

app.core.addRegions({
	rootRegion: '.site'
});


app.core.addInitializer(function(options) {
	// init
});


app.core.on('start', function(options) {

	this.controller = new Controller(this);
	this.router = new Router({controller: this.controller});

	if (Backbone.history) {
		Backbone.history.start({root:'dist'});
	}

});
},{"./controller":5,"./dataProxy":6,"./router":9}],2:[function(require,module,exports){

var CategoryModel = Backbone.Model;

var CategoryCollection = Backbone.Collection.extend({
    
    model: CategoryModel,

    url: function() {
        return '/api/term/category';
    },
    
    parse: function(data) {
        var filtered = [];
        _.each(data, function(i) {
            if (i.count > 0) {
                filtered.push(i)
            }
        });
        
        return filtered;
    }

});

module.exports = CategoryCollection;
},{}],3:[function(require,module,exports){

var Posts = Backbone.Collection.extend({
    
    url: function() {
        // return '/api/posts';
        return '/wp/wp-json/posts';
    },

    category_filter: function(category_id) {
        var filtered = _.filter(this.toJSON(), function(m) {
            var filter = false
            _.each(m.terms.category, function(c) {
                if (c.slug === category_id) {
                    filter = true;
                }
            })

            return filter;
        });

        return new Backbone.Collection( filtered );
    },

    wordy_filter: function(wordy_id) {
        var filtered = _.filter(this.toJSON(), function(m) {
            var filter = false
            _.each(m.terms.wordy, function(c) {
                if (c.slug === wordy_id) {
                    filter = true;
                }
            })

            return filter;
        });

        return new Backbone.Collection( filtered );
    }

});

module.exports = Posts;
},{}],4:[function(require,module,exports){

var WordyModel = Backbone.Model;

var WordyCollection = Backbone.Collection.extend({
    
    model: WordyModel,

    url: function() {
        return '/api/term/wordy';
    },

    parse: function(data) {
        var filtered = [];
        _.each(data, function(i) {
            if (i.count > 0) {
                filtered.push(i)
            }
        });
        
        return filtered;
    }

});

module.exports = WordyCollection;
},{}],5:[function(require,module,exports){

Layout = require('./views/mainLayoutView');

module.exports = Controller = Marionette.Controller.extend({
   
    initialize: function(app) {
        this.layout = new Layout();
        app.rootRegion.show( this.layout );
    },

    home: function() {
        this.layout.home();
    },

    post: function(id) {
        this.layout.post(id)
    },

    category: function(id) {
        this.layout.category(id);
    },

    wordy: function(id) {
        this.layout.wordies(id);  
    }

});
},{"./views/mainLayoutView":23}],6:[function(require,module,exports){

var PostModel             = require('./model/postModel'),
    PostsCollection       = require('./collections/postsCollection'),
    CategoryCollection    = require('./collections/categoryCollection'),
    WordyCollection       = require('./collections/wordyCollection'),

    postModel             = new PostModel(),
    postsCollection       = new PostsCollection(),
    categoryCollection    = new CategoryCollection(),
    wordyCollection       = new WordyCollection();


module.exports = _.extend({

    categoriesReady: false,
    wordiesReady: false,
    postsReady: false,

    isReady: function() {
        if (this.categoriesReady && this.wordiesReady && this.postsReady) {
            this.trigger('dataReady');
        }
    },

    fetch: function() {
        var that = this;
        categoryCollection.fetch({
            success: function() {
                that.categoriesReady = true;
                that.isReady();
            }
        });

        wordyCollection.fetch({
            success: function() {
                that.wordiesReady = true;
                that.isReady();
            }
        });

        postsCollection.fetch({
            success: function() {
                that.postsReady = true;
                that.isReady();
            }
        });
    },

    getCategories: function(where) {
        if (where) {
            return new Backbone.Collection(categoryCollection.where(where));
        }
        else {
            return categoryCollection;
        }
    },

    getWordies: function() {
        return wordyCollection;
    },

    getPosts: function() {
        window.postsCollection = postsCollection;
        return postsCollection;
    },

    getPostsByCategory: function(category_id) {
        return postsCollection.category_filter(category_id);
    },


    getPostsByWordies: function(category_id) {
        return postsCollection.wordy_filter(category_id);
    },

    getPostById: function(id, callback) {
        postModel.fetch({
            url: '/wp/wp-json/posts/'+id,
            success: function(m) {
                callback(m)
            }
        })
    }


}, Backbone.Events);
},{"./collections/categoryCollection":2,"./collections/postsCollection":3,"./collections/wordyCollection":4,"./model/postModel":8}],7:[function(require,module,exports){

var app = require('./app');

app.start();

},{"./app":1}],8:[function(require,module,exports){

module.exports = PostModel = Backbone.Model.extend({
	// url: _CONFIG.base_url+'/api/posts'
});


},{}],9:[function(require,module,exports){

module.exports = Router = Marionette.AppRouter.extend({
   
    appRoutes: {
        ""                   :   "home",
        "posts/:id"          :   "post",
        "posts/:id/:slug"    :   "post",
        "category/:id"       :   "category",
        "wordy/:id"          :   "wordy"
    }

});
},{}],10:[function(require,module,exports){
module.exports = function(obj){
var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};
with(obj||{}){
__p+='\n<div class="page-heading '+
((__t=(slug))==null?'':__t)+
'">\n    <div class="inner">\n        <h2>'+
((__t=(name))==null?'':__t)+
'</h2>\n    </div>\n</div>\n\n\n<div class="posts category-posts">\n    \n    <div class="main-block">\n\n        <aside class="right-side">\n\n            <div class="trending-stories wordies '+
((__t=(slug))==null?'':__t)+
'">\n                <div class="topic '+
((__t=(slug))==null?'':__t)+
'">'+
((__t=(name))==null?'':__t)+
'</div>\n                <h3 class="section-heading">WORDIES</h3>\n                <div class="links">\n                    ';
 _.each(wordies, function(i) { 
__p+='\n                        <div class="link">\n                            <a href="#wordy/'+
((__t=(i.slug))==null?'':__t)+
'">'+
((__t=(i.name))==null?'':__t)+
' ( '+
((__t=(i.count))==null?'':__t)+
' )</a>\n                        </div>\n                    ';
 }); 
__p+='\n                    <!--<div class="link">\n                        <a href="#">All ( 6 )</a>\n                    </div>\n                    <div class="link">\n                        <a href="#">You May Be Cousins ( 4 )</a>\n                    </div>\n                    <div class="link">\n                        <a href="#">Crate Digging ( 2 )</a>\n                    </div>-->\n                </div>\n            </div>\n\n\n            <div class="ad2"></div>\n\n        </aside>\n\n\n\n        ';
 _.each(items, function(i) { 
__p+='\n        <article class="post item1">\n                \n            <div class="media">\n                \n                <a href="#posts/'+
((__t=(i.ID))==null?'':__t)+
'/'+
((__t=(i.slug))==null?'':__t)+
'">\n                    <img src="'+
((__t=(i.featured_image.source))==null?'':__t)+
'" alt="" width="100%">\n                </a>\n            </div>\n\n            <div class="meta">\n                <a href="#'+
((__t=(i.link))==null?'':__t)+
'" class="title">\n                    <h1>'+
((__t=(i.title))==null?'':__t)+
'</h1>\n                </a>\n                <div class="author-date">\n                    <span class="author">By \n                        <a href="#">'+
((__t=(i.author.display_name))==null?'':__t)+
'</a>\n                    </span>\n                    <span class="date">on '+
((__t=(i.date))==null?'':__t)+
'</span>\n                </div>\n                <div class="excerpt">\n                    <p>'+
((__t=(i.excerpt))==null?'':__t)+
'</p>\n                </div>\n            </div>\n        </article>\n        ';
 }); 
__p+='\n\n\n    </div>\n\n\n\n\n\n\n\n\n\n</div>\n\n</div>\n\n';
}
return __p;
};

},{}],11:[function(require,module,exports){
module.exports = function(obj){
var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};
with(obj||{}){
__p+='        <div class="inner">\n            \n            <ul>\n                <li><a href="#">ABOUT</a></li>\n                <li><a href="#">CONTACT</a></li>\n                <li><a href="#"><img src="images/wordy-logo-light.png" width="100"></a></li>\n                <li><a href="#">TERMS OF USE</a></li>\n            </ul>\n\n        </div>';
}
return __p;
};

},{}],12:[function(require,module,exports){
module.exports = function(obj){
var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};
with(obj||{}){
__p+='\n    ';
 if (featured_image) { 
__p+='\n    <div class="media">\n        ';
 if (terms.category) { 
__p+='\n        <div class="category">\n            <a href="#category/'+
((__t=(terms.category[0].slug))==null?'':__t)+
'" class="'+
((__t=(terms.category[0].slug))==null?'':__t)+
'">'+
((__t=(terms.category[0].name))==null?'':__t)+
'</a>\n        </div>\n        ';
 } 
__p+='\n        <a href="#posts/'+
((__t=(ID))==null?'':__t)+
'/'+
((__t=(slug))==null?'':__t)+
'">\n            <img src="'+
((__t=(featured_image.source))==null?'':__t)+
'" alt="" width="100%"></a>\n    </div>\n    ';
 } 
__p+='\n\n    <div class="meta">\n        <a href="#posts/'+
((__t=(ID))==null?'':__t)+
'/'+
((__t=(slug))==null?'':__t)+
'" class="title">\n            <h1>'+
((__t=(title))==null?'':__t)+
'</h1></a>\n        <div class="author-date">\n            <span class="author">By <a href="#">'+
((__t=(author.name))==null?'':__t)+
'</a></span>\n            ';
 var date = new Date(date); 
__p+='\n            <span class="date">on '+
((__t=(date))==null?'':__t)+
'</span>\n        </div>\n        <div class="excerpt">\n            <p>'+
((__t=(excerpt))==null?'':__t)+
'</p>\n        </div>\n    </div>\n    <a href="#posts/'+
((__t=(ID))==null?'':__t)+
'/'+
((__t=(slug))==null?'':__t)+
'" class="more">Read More</a>';
}
return __p;
};

},{}],13:[function(require,module,exports){
module.exports = function(obj){
var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};
with(obj||{}){
__p+='        <div class="featured menu-anim">\n            \n            <div class="hero-section">\n\n                ';
 _.each(featured, function(i, index) { 
__p+='\n\n                <a href="#posts/'+
((__t=(i.get('ID')))==null?'':__t)+
'/'+
((__t=(i.get('slug')))==null?'':__t)+
'" class="item item'+
((__t=(index))==null?'':__t)+
'" style="background-image:url('+
((__t=(i.get('featured_image').source))==null?'':__t)+
')">\n\n                    <div class="content">\n                        <h1>'+
((__t=(i.get('title')))==null?'':__t)+
'</h1>\n                    </div>\n                </a>\n\n                ';
 }); 
__p+='\n\n            </div>\n\n\n        </div>\n\n        <!--<div class="main-nav nav2 menu-anim">\n            <nav>\n            <ul>\n                <li><a href="comics.html" class="comics">Comics</a></li>\n                ';
 _.each(categories, function(i) { 
__p+='\n                <li><a href="'+
((__t=(i.slug))==null?'':__t)+
'" class="'+
((__t=(i.slug))==null?'':__t)+
'">'+
((__t=(i.name))==null?'':__t)+
'</a></li>\n                ';
 }); 
__p+='\n            </ul>\n            </nav>\n        </div>-->';
}
return __p;
};

},{}],14:[function(require,module,exports){
module.exports = function(obj){
var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};
with(obj||{}){
__p+='            <div class="posts">\n                \n                <div class="main-block">\n\n                    ';
 _.each(_.range(3,4), function(i) { 
__p+='\n                    <article class="post item1">\n                        ';
 if (items[i].featured_image) { 
__p+='\n                        <div class="media">\n                            ';
 if (items[i].terms.category) { 
__p+='\n                            <div class="category">\n                                <a href="#category/'+
((__t=(items[i].terms.category[0].slug))==null?'':__t)+
'" class="'+
((__t=(items[i].terms.category[0].slug))==null?'':__t)+
'">'+
((__t=(items[i].terms.category[0].name))==null?'':__t)+
'</a>\n                            </div>\n                            ';
 } 
__p+='\n                            <a href="#posts/'+
((__t=(items[i].ID))==null?'':__t)+
'/'+
((__t=(items[i].slug))==null?'':__t)+
'">\n                                <img src="'+
((__t=(items[i].featured_image.source))==null?'':__t)+
'" alt="" width="100%"></a>\n                        </div>\n                        ';
 } 
__p+='\n\n                        <div class="meta">\n                            <a href="#posts/'+
((__t=(items[i].ID))==null?'':__t)+
'/'+
((__t=(items[i].slug))==null?'':__t)+
'" class="title">\n                                <h1>'+
((__t=(items[i].title))==null?'':__t)+
'</h1></a>\n                            <div class="author-date">\n                                <span class="author">By <a href="#">'+
((__t=(items[i].author.name))==null?'':__t)+
'</a></span>\n                                ';
 var date = new Date(items[i].date); 
__p+='\n                                <span class="date">on '+
((__t=(date))==null?'':__t)+
'</span>\n                            </div>\n                            <div class="excerpt">\n                                <p>'+
((__t=(items[i].excerpt))==null?'':__t)+
'</p>\n                            </div>\n                        </div>\n                        <a href="#posts/'+
((__t=(items[i].ID))==null?'':__t)+
'/'+
((__t=(items[i].slug))==null?'':__t)+
'" class="more">Read More</a>\n                    </article>\n                    ';
 }); 
__p+='\n\n                    <div class="trending-stories">\n                        <h3 class="section-heading">TRENDING STORIES</h3>\n                        <div class="links">\n                            ';
 _.each(_.range(5,10), function(i) { 
__p+='\n                            <div class="link">\n                                <a href="#">'+
((__t=(items[i].title))==null?'':__t)+
'</a>\n                            </div>\n                            ';
 }); 
__p+='\n                        </div>\n                    </div>\n\n\n                    <div class="ad2"></div>\n\n\n                </div>\n\n\n\n\n\n\n\n                <div class="row">\n\n                    ';
 _.each(_.range(4,6), function(i) { 
__p+='\n                    <article class="post item3">\n                        ';
 if (items[i].featured_image) { 
__p+='\n                        <div class="media">\n                            ';
 if (items[i].terms.category) { 
__p+='\n                            <div class="category">\n                                <a href="#category/'+
((__t=(items[i].terms.category[0].slug))==null?'':__t)+
'" class="'+
((__t=(items[i].terms.category[0].slug))==null?'':__t)+
'">'+
((__t=(items[i].terms.category[0].name))==null?'':__t)+
'</a>\n                            </div>\n                            ';
 } 
__p+='\n                            <a href="#posts/'+
((__t=(items[i].ID))==null?'':__t)+
'/'+
((__t=(items[i].slug))==null?'':__t)+
'">\n                                <img src="'+
((__t=(items[i].featured_image.source))==null?'':__t)+
'" alt="" width="100%"></a>\n                        </div>\n                        ';
 } 
__p+='\n\n                        <div class="meta">\n                            <a href="#posts/'+
((__t=(items[i].ID))==null?'':__t)+
'/'+
((__t=(items[i].slug))==null?'':__t)+
'" class="title">\n                                <h1>'+
((__t=(items[i].title))==null?'':__t)+
'</h1></a>\n                            <div class="author-date">\n                                <span class="author">By <a href="#">'+
((__t=(items[i].author.name))==null?'':__t)+
'</a></span>\n                                ';
 var date = new Date(items[i].date); 
__p+='\n                                <span class="date">on '+
((__t=(date))==null?'':__t)+
'</span>\n                            </div>\n                            <div class="excerpt">\n                                <p>'+
((__t=(items[i].excerpt))==null?'':__t)+
'</p>\n                            </div>\n                        </div>\n                        <a href="#posts/'+
((__t=(items[i].ID))==null?'':__t)+
'/'+
((__t=(items[i].slug))==null?'':__t)+
'" class="more">Read More</a>\n                    </article>\n                    ';
 }); 
__p+='\n\n                </div>\n\n\n\n\n\n                <div class="row">\n\n                    ';
 _.each(_.range(6,12), function(i) { 
__p+='\n                    <article class="post item2">\n                        ';
 if (items[i].featured_image) { 
__p+='\n                        <div class="media">\n                            ';
 if (items[i].terms.category) { 
__p+='\n                            <div class="category">\n                                <a href="#category/'+
((__t=(items[i].terms.category[0].slug))==null?'':__t)+
'" class="'+
((__t=(items[i].terms.category[0].slug))==null?'':__t)+
'">'+
((__t=(items[i].terms.category[0].name))==null?'':__t)+
'</a>\n                            </div>\n                            ';
 } 
__p+='\n                            <a href="#posts/'+
((__t=(items[i].ID))==null?'':__t)+
'/'+
((__t=(items[i].slug))==null?'':__t)+
'">\n                                <img src="'+
((__t=(items[i].featured_image.source))==null?'':__t)+
'" alt="" width="100%"></a>\n                        </div>\n                        ';
 } 
__p+='\n\n                        <div class="meta">\n                            <a href="#posts/'+
((__t=(items[i].ID))==null?'':__t)+
'/'+
((__t=(items[i].slug))==null?'':__t)+
'" class="title">\n                                <h1>'+
((__t=(items[i].title))==null?'':__t)+
'</h1></a>\n                            <div class="author-date">\n                                <span class="author">By <a href="#">'+
((__t=(items[i].author.name))==null?'':__t)+
'</a></span>\n                                ';
 var date = new Date(items[i].date); 
__p+='\n                                <span class="date">on '+
((__t=(date))==null?'':__t)+
'</span>\n                            </div>\n                            <div class="excerpt">\n                                <p>'+
((__t=(items[i].excerpt))==null?'':__t)+
'</p>\n                            </div>\n                        </div>\n                        <a href="#posts/'+
((__t=(items[i].ID))==null?'':__t)+
'/'+
((__t=(items[i].slug))==null?'':__t)+
'" class="more">Read More</a>\n                    </article>\n                    ';
 }); 
__p+='\n\n                </div>\n\n\n\n                <div class="more-posts">\n\n                </div>\n\n\n\n                <button class="load-more">LOAD MORE</button>\n\n\n\n            </div>';
}
return __p;
};

},{}],15:[function(require,module,exports){
module.exports = function(obj){
var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};
with(obj||{}){
__p+='\n        <div class="topnav">\n\n        </div>\n\n        <div class="side-menu menu-anim">\n\n        </div>\n        <div class="menu-overlay"></div>\n\n\n        <div class="home-content">\n\n        </div>\n\n        <div class="ad"></div>\n\n        <div class="main-content">\n\n\n        </div>\n\n\n        <div class="main-footer">\n            \n        </div>';
}
return __p;
};

},{}],16:[function(require,module,exports){
module.exports = function(obj){
var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};
with(obj||{}){
__p+='\n            <div class="posts">\n                \n                <div class="main-block">\n\n                    <article class="post item1">\n                        \n                        <h1>'+
((__t=(title))==null?'':__t)+
'</h1>\n\n                        <div class="meta">\n                            \n                            <div class="author-date">\n                                <span class="author">By <a href="#">'+
((__t=(author.name))==null?'':__t)+
'</a></span>\n                                <span class="date">on '+
((__t=(date))==null?'':__t)+
'</span>\n                            </div>\n                            <div class="content">\n                                '+
((__t=(content))==null?'':__t)+
'\n                            </div>\n                        </div>\n                    </article>\n\n\n                    <div class="trending-stories">\n                        <h3 class="section-heading">RELATED STORIES</h3>\n                        <div class="links">\n                            ';
 _.each(related, function(i) { 
__p+='\n                            <div class="link">\n                                <a href="#posts/'+
((__t=(i.ID))==null?'':__t)+
'/'+
((__t=(i.slug))==null?'':__t)+
'">'+
((__t=(i.title))==null?'':__t)+
'</a>\n                            </div>\n                            ';
 }); 
__p+='\n                        </div>\n                    </div>\n\n                </div>\n\n\n            </div>';
}
return __p;
};

},{}],17:[function(require,module,exports){
module.exports = function(obj){
var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};
with(obj||{}){
__p+='\n        <div class="logo">\n            <img src="images/wordy-logo.png" alt="">\n        </div>\n\n        <div class="wordies">\n            <h3>WORDIES</h3>\n            <ul>\n                ';
 _.each(wordies, function(i) { 
__p+='\n                <li><a href="#wordy/'+
((__t=(i.slug))==null?'':__t)+
'">'+
((__t=(i.name))==null?'':__t)+
'</a></li>\n                ';
 }); 
__p+='\n            </ul>\n        </div>\n\n        <div class="main">\n            <h3>CATEGORIES</h3>\n            <ul>\n                ';
 _.each(categories, function(i) { 
__p+='\n                <li class="'+
((__t=(i.slug))==null?'':__t)+
'"><a href="#category/'+
((__t=(i.slug))==null?'':__t)+
'">'+
((__t=(i.name))==null?'':__t)+
'</a></li>\n                ';
 }); 
__p+='\n            </ul>\n        </div>\n';
}
return __p;
};

},{}],18:[function(require,module,exports){
module.exports = function(obj){
var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};
with(obj||{}){
__p+='\n        <div class="menu-icon menu-anim"><span></span></div>\n\n        <a href="index.html" class="text-logo">WORDY SANCHEZ</a>\n\n        <div class="main-nav nav1 menu-anim on">\n            <nav>\n                <ul>\n                    ';
 _.each(menu, function(i) { 
__p+='\n                        <li class="'+
((__t=(i.slug))==null?'':__t)+
'">\n                            <a href="'+
((__t=(i.slug))==null?'':__t)+
'" class="'+
((__t=(i.slug))==null?'':__t)+
'">'+
((__t=(i.name))==null?'':__t)+
'</a>\n                        </li>\n                    ';
 }) 
__p+='\n    \n                        <!-- <div class="dd-menu">\n                            <ul>\n                                <li><a href="cosplay" class="cosplay">Cosplay</a></li>\n                                <li><a href="anime" class="anime">Anime</a></li>\n                                <li><a href="gadgets" class="gadgets">Gadgets</a></li>\n                                <li><a href="screenings" class="screenings">Screenings</a></li>\n                                <li><a href="features" class="features">Features</a></li>\n                            </ul>\n                        </div> -->\n                    </li>\n                </ul>\n            </nav>\n        </div>\n\n        <div class="top-right">\n            <div class="social">\n                <div class="icon icon-facebook"></div>\n                <div class="icon icon-twitter"></div>\n                <div class="icon icon-instagram"></div>\n                <div class="icon icon-tumblr"></div>\n            </div>\n\n            <!--<div class="search-box">\n                <input type="text" id="search" placeholder="SEARCH">\n                <span class="icon-search"></span>\n            </div>-->\n        </div>\n\n';
}
return __p;
};

},{}],19:[function(require,module,exports){

var template = require('../../templates/category/layout.html');

module.exports = Marionette.LayoutView.extend({
    template: template,

    onShow: function() {
        $(window).on('scroll', this.onScroll);
        this.onScroll();

        $(window).on('resize', this.onResize);
        this.onResize();

        $(window).scrollTop(0);
    },

    onScroll: function(e) {
        var _t = $(window).scrollTop();

        if (_t >= 55) {
            $('.right-side').addClass('fixed');
        }
        else {
            $('.right-side').removeClass('fixed');
        }
    },

    onResize: function(e) {
        var $mainW = $('.main-block').width(),
            $windowW = $(window).width(),
            percent = 38,
            width = $mainW / 100 * percent;

        $('.right-side').css({
            'right' : ($windowW - $mainW) / 2,
            'width' : $mainW / 100 * percent
        });
    }
});
},{"../../templates/category/layout.html":10}],20:[function(require,module,exports){

var template = require('../templates/footer.html');

module.exports = FooterView = Marionette.ItemView.extend({
    template: template
});
},{"../templates/footer.html":11}],21:[function(require,module,exports){

var template = require('../../templates/home/featured.html');

module.exports = FeaturedView = Marionette.ItemView.extend({
    template: template,

    initialize: function() {
        // $(window).on('scroll', this.onScroll);
    },


    // onRender: function() {
    //     // this.onScroll();
    // },

    // onScroll: function(e) {
    //     // var _t = $(window).scrollTop();

    //     // if (_t >= 440) {
    //     //     // $('.main-nav').addClass('fixed');
    //     //     $('.main-nav.nav1').addClass('on');
    //     //     $('.main-nav.nav2').removeClass('on');
    //     // }
    //     // else {
    //     //     // $('.main-nav').removeClass('fixed');
    //     //     $('.main-nav.nav2').addClass('on');
    //     //     $('.main-nav.nav1').removeClass('on');
    //     // }
    // }

});
},{"../../templates/home/featured.html":13}],22:[function(require,module,exports){

var template = require('../../templates/home/posts.html'),
    articleTpl = require('../../templates/home/article.html'),
    dataProxy = require('../../dataProxy');

var Article = Marionette.ItemView.extend({
    tagName: 'article',
    className: 'post item2',
    template: articleTpl
});

var Articles = Marionette.CollectionView.extend({
    className: 'row',
    childView: Article,
});

module.exports = Marionette.LayoutView.extend({
    template: template,

    postOffset: 12,
    loadThisMore : 6,

    moreCollection: new Backbone.Collection(),

    regions: {
        'moreRegion' : '.more-posts'
    },

    onShow: function() {
        var that = this;
        $('.main-nav a').removeClass('active');

        this.moreCollection = new Backbone.Collection();
        this.moreViews = new Articles({
            collection: this.moreCollection
        })

        this.moreRegion.show( this.moreViews );

        thispostOffset = 12;
        this.resetPosts();
    },

    onDestroy: function() {
        // dataProxy.getPosts().each(function(m, i) {
        //     m.set('loaded', false)
        // });
    },

    events: {
        'click .load-more' : 'loadMore'
    },

    loadMore: function() {
        
        var data = dataProxy.getPosts().where({'loaded':false}).slice(0, this.loadThisMore);

        if (_.isEmpty(data)) {
            return $('.load-more').fadeOut();
        }

        this.moreCollection.add(data);

        this.postOffset += this.loadThisMore;
        this.resetPosts();
    },

    resetPosts: function() {
        var that = this;
        dataProxy.getPosts().each(function(m, i) {
            if (i < that.postOffset) {
                m.set('loaded', true);
            }
            else {
                m.set('loaded', false);
            }
        });
    }
});
},{"../../dataProxy":6,"../../templates/home/article.html":12,"../../templates/home/posts.html":14}],23:[function(require,module,exports){

var template = require('../templates/main-layout.html'),
    dataProxy = require('../dataProxy'),

    // Views
    TopnavView       = require('./topnavView'),
    SidenavView      = require('./sidenavView'),
    FooterView       = require('./footerView'),

    HomefeaturedView = require('./home/featuredView'),

    HomeLayout       = require('./home/homeLayout'),

    PostView         = require('./postView'),

    CategoryLayout   = require('./category/categoryLayout');


module.exports = MainLayout = Marionette.LayoutView.extend({

    template: template,

    views: {},

    regions: {
        topnavRegion      : '.topnav',
        sidenavRegion     : '.side-menu',
        footerRegion      : '.main-footer',

        featuredRegion    : '.home-content',
        mainContentRegion : '.main-content'
    },

    events: {
        'click .menu-icon, .menu-overlay' : 'toggleMenu'
    },

    onRender: function() {

        // topnav
        var menuModel = new Backbone.Model({
            'menu': dataProxy.getCategories({'main-menu':'checked'}).toJSON()
        });
        this.views.topnav = new TopnavView({model: menuModel});
        this.topnavRegion.show( this.views.topnav );

        // sidenav
        var sidenavModel = new Backbone.Model({
            'categories': dataProxy.getCategories().toJSON(),
            'wordies': dataProxy.getWordies().toJSON()
        });
        this.views.sidenav = new SidenavView({model: sidenavModel});
        this.sidenavRegion.show( this.views.sidenav );

        this.views.footer = new FooterView();
        this.footerRegion.show( this.views.footer );

    },


    home: function() {

        // featured
        var featured = dataProxy.getPosts().filter(function(model) {
            var f = model.get('featured_image');
            return ((typeof(f) !== 'undefined') && (f!==null));
        });

        var featuredModel = new Backbone.Model({
            'categories': dataProxy.getCategories().toJSON(),
            'featured' : featured.slice(0, 3)
        });
        this.views.homefeaturedView = new HomefeaturedView({model: featuredModel});
        this.featuredRegion.show( this.views.homefeaturedView );


        // home
        this.views.homeLayout = new HomeLayout({
            collection: dataProxy.getPosts()
        });
        this.mainContentRegion.show( this.views.homeLayout );
    },


    post: function(id) {
        var that = this;
        if (this.views.homefeaturedView) {
            this.views.homefeaturedView.destroy();
        }
        
        dataProxy.getPostById(id, function(m) {
            var model = m;
            var category_id = model.get('terms').category[0].slug;
            var related = dataProxy.getPostsByCategory(category_id).toJSON();
            model.set('related', related);
            
            that.views.post = new PostView({
                model: model
            });
            that.mainContentRegion.show( that.views.post );
        });
    },


    category: function(id) {
        var that = this;
        if (this.views.homefeaturedView) {
            this.views.homefeaturedView.destroy();
        }

        var model = dataProxy.getCategories().findWhere({'slug': id});
        model.set('items', dataProxy.getPostsByCategory(id).toJSON());
        model.set('wordies', dataProxy.getWordies().toJSON());

        // category
        this.views.categoryLayout = new CategoryLayout({
            model: model
        });
        this.mainContentRegion.show( this.views.categoryLayout );
    },


    wordies: function(id) {
        var that = this;
        if (this.views.homefeaturedView) {
            this.views.homefeaturedView.destroy();
        }

        var model = dataProxy.getWordies().findWhere({'slug': id});
        model.set('items', dataProxy.getPostsByWordies(id).toJSON());
        model.set('wordies', dataProxy.getWordies().toJSON());

        // category
        this.views.categoryLayout = new CategoryLayout({
            model: model
        });
        this.mainContentRegion.show( this.views.categoryLayout );
    },


    toggleMenu: function(e) {
        if ($('.menu-anim').hasClass('animate'))
        {
            $('.menu-anim').removeClass('animate');
            $('body').removeClass('menu-on');
        }
        else {
            $('.menu-anim').addClass('animate');
            $('body').addClass('menu-on');
        }
    }


});
},{"../dataProxy":6,"../templates/main-layout.html":15,"./category/categoryLayout":19,"./footerView":20,"./home/featuredView":21,"./home/homeLayout":22,"./postView":24,"./sidenavView":25,"./topnavView":26}],24:[function(require,module,exports){

var template = require('../templates/post.html');

module.exports = PostView = Marionette.ItemView.extend({
    template: template,

    onShow: function() {
        $(window).scrollTop(0);
    }

});
},{"../templates/post.html":16}],25:[function(require,module,exports){

var template = require('../templates/sidenav.html');

module.exports = SidenavView = Marionette.ItemView.extend({
    template: template,

    events: {
        'click li a' : 'closeMenu'
    },

    closeMenu: function() {
        $('.menu-icon').click();
    }
});
},{"../templates/sidenav.html":17}],26:[function(require,module,exports){

var template = require('../templates/topnav.html');

module.exports = TopnavView = Marionette.ItemView.extend({
    template: template,

    events: {
        'click .text-logo' : 'goHome',
        'click .main-nav a' : 'gotoCategory'
    },

    goHome: function(e) {
        e.preventDefault();
        Backbone.history.navigate('', true);
    },


    gotoCategory: function(e) {
        e.preventDefault();
        var $target = $(e.currentTarget);
        var href = $target.attr('href');
        Backbone.history.navigate('category/'+href, true);

        $('.main-nav a').removeClass('active');
        $target.addClass('active');
    }
});
},{"../templates/topnav.html":18}]},{},[7])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9lcmlja3NvdG8vRGV2ZWxvcG1lbnQvaGIvd29yZHkvd29yZHlzYW5jaGV6L25vZGVfbW9kdWxlcy9ndWxwLWJyb3dzZXJpZnkvbm9kZV9tb2R1bGVzL2Jyb3dzZXJpZnkvbm9kZV9tb2R1bGVzL2Jyb3dzZXItcGFjay9fcHJlbHVkZS5qcyIsIi9Vc2Vycy9lcmlja3NvdG8vRGV2ZWxvcG1lbnQvaGIvd29yZHkvd29yZHlzYW5jaGV6L3NyYy9zY3JpcHRzL2FwcC5qcyIsIi9Vc2Vycy9lcmlja3NvdG8vRGV2ZWxvcG1lbnQvaGIvd29yZHkvd29yZHlzYW5jaGV6L3NyYy9zY3JpcHRzL2NvbGxlY3Rpb25zL2NhdGVnb3J5Q29sbGVjdGlvbi5qcyIsIi9Vc2Vycy9lcmlja3NvdG8vRGV2ZWxvcG1lbnQvaGIvd29yZHkvd29yZHlzYW5jaGV6L3NyYy9zY3JpcHRzL2NvbGxlY3Rpb25zL3Bvc3RzQ29sbGVjdGlvbi5qcyIsIi9Vc2Vycy9lcmlja3NvdG8vRGV2ZWxvcG1lbnQvaGIvd29yZHkvd29yZHlzYW5jaGV6L3NyYy9zY3JpcHRzL2NvbGxlY3Rpb25zL3dvcmR5Q29sbGVjdGlvbi5qcyIsIi9Vc2Vycy9lcmlja3NvdG8vRGV2ZWxvcG1lbnQvaGIvd29yZHkvd29yZHlzYW5jaGV6L3NyYy9zY3JpcHRzL2NvbnRyb2xsZXIuanMiLCIvVXNlcnMvZXJpY2tzb3RvL0RldmVsb3BtZW50L2hiL3dvcmR5L3dvcmR5c2FuY2hlei9zcmMvc2NyaXB0cy9kYXRhUHJveHkuanMiLCIvVXNlcnMvZXJpY2tzb3RvL0RldmVsb3BtZW50L2hiL3dvcmR5L3dvcmR5c2FuY2hlei9zcmMvc2NyaXB0cy9mYWtlX2NlZTM3MjJkLmpzIiwiL1VzZXJzL2VyaWNrc290by9EZXZlbG9wbWVudC9oYi93b3JkeS93b3JkeXNhbmNoZXovc3JjL3NjcmlwdHMvbW9kZWwvcG9zdE1vZGVsLmpzIiwiL1VzZXJzL2VyaWNrc290by9EZXZlbG9wbWVudC9oYi93b3JkeS93b3JkeXNhbmNoZXovc3JjL3NjcmlwdHMvcm91dGVyLmpzIiwiL1VzZXJzL2VyaWNrc290by9EZXZlbG9wbWVudC9oYi93b3JkeS93b3JkeXNhbmNoZXovc3JjL3NjcmlwdHMvdGVtcGxhdGVzL2NhdGVnb3J5L2xheW91dC5odG1sIiwiL1VzZXJzL2VyaWNrc290by9EZXZlbG9wbWVudC9oYi93b3JkeS93b3JkeXNhbmNoZXovc3JjL3NjcmlwdHMvdGVtcGxhdGVzL2Zvb3Rlci5odG1sIiwiL1VzZXJzL2VyaWNrc290by9EZXZlbG9wbWVudC9oYi93b3JkeS93b3JkeXNhbmNoZXovc3JjL3NjcmlwdHMvdGVtcGxhdGVzL2hvbWUvYXJ0aWNsZS5odG1sIiwiL1VzZXJzL2VyaWNrc290by9EZXZlbG9wbWVudC9oYi93b3JkeS93b3JkeXNhbmNoZXovc3JjL3NjcmlwdHMvdGVtcGxhdGVzL2hvbWUvZmVhdHVyZWQuaHRtbCIsIi9Vc2Vycy9lcmlja3NvdG8vRGV2ZWxvcG1lbnQvaGIvd29yZHkvd29yZHlzYW5jaGV6L3NyYy9zY3JpcHRzL3RlbXBsYXRlcy9ob21lL3Bvc3RzLmh0bWwiLCIvVXNlcnMvZXJpY2tzb3RvL0RldmVsb3BtZW50L2hiL3dvcmR5L3dvcmR5c2FuY2hlei9zcmMvc2NyaXB0cy90ZW1wbGF0ZXMvbWFpbi1sYXlvdXQuaHRtbCIsIi9Vc2Vycy9lcmlja3NvdG8vRGV2ZWxvcG1lbnQvaGIvd29yZHkvd29yZHlzYW5jaGV6L3NyYy9zY3JpcHRzL3RlbXBsYXRlcy9wb3N0Lmh0bWwiLCIvVXNlcnMvZXJpY2tzb3RvL0RldmVsb3BtZW50L2hiL3dvcmR5L3dvcmR5c2FuY2hlei9zcmMvc2NyaXB0cy90ZW1wbGF0ZXMvc2lkZW5hdi5odG1sIiwiL1VzZXJzL2VyaWNrc290by9EZXZlbG9wbWVudC9oYi93b3JkeS93b3JkeXNhbmNoZXovc3JjL3NjcmlwdHMvdGVtcGxhdGVzL3RvcG5hdi5odG1sIiwiL1VzZXJzL2VyaWNrc290by9EZXZlbG9wbWVudC9oYi93b3JkeS93b3JkeXNhbmNoZXovc3JjL3NjcmlwdHMvdmlld3MvY2F0ZWdvcnkvY2F0ZWdvcnlMYXlvdXQuanMiLCIvVXNlcnMvZXJpY2tzb3RvL0RldmVsb3BtZW50L2hiL3dvcmR5L3dvcmR5c2FuY2hlei9zcmMvc2NyaXB0cy92aWV3cy9mb290ZXJWaWV3LmpzIiwiL1VzZXJzL2VyaWNrc290by9EZXZlbG9wbWVudC9oYi93b3JkeS93b3JkeXNhbmNoZXovc3JjL3NjcmlwdHMvdmlld3MvaG9tZS9mZWF0dXJlZFZpZXcuanMiLCIvVXNlcnMvZXJpY2tzb3RvL0RldmVsb3BtZW50L2hiL3dvcmR5L3dvcmR5c2FuY2hlei9zcmMvc2NyaXB0cy92aWV3cy9ob21lL2hvbWVMYXlvdXQuanMiLCIvVXNlcnMvZXJpY2tzb3RvL0RldmVsb3BtZW50L2hiL3dvcmR5L3dvcmR5c2FuY2hlei9zcmMvc2NyaXB0cy92aWV3cy9tYWluTGF5b3V0Vmlldy5qcyIsIi9Vc2Vycy9lcmlja3NvdG8vRGV2ZWxvcG1lbnQvaGIvd29yZHkvd29yZHlzYW5jaGV6L3NyYy9zY3JpcHRzL3ZpZXdzL3Bvc3RWaWV3LmpzIiwiL1VzZXJzL2VyaWNrc290by9EZXZlbG9wbWVudC9oYi93b3JkeS93b3JkeXNhbmNoZXovc3JjL3NjcmlwdHMvdmlld3Mvc2lkZW5hdlZpZXcuanMiLCIvVXNlcnMvZXJpY2tzb3RvL0RldmVsb3BtZW50L2hiL3dvcmR5L3dvcmR5c2FuY2hlei9zcmMvc2NyaXB0cy92aWV3cy90b3BuYXZWaWV3LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMzQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDeEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDeENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3hCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDMUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDckZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDSkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ0xBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNYQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDL0NBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDN0NBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDL0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDM0lBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN6QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN6QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNuQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3RDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDTEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDOUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN6SkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNWQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpfXZhciBmPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChmLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGYsZi5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJcbnZhciBkYXRhUHJveHkgID0gcmVxdWlyZSgnLi9kYXRhUHJveHknKSxcblx0Um91dGVyICAgICA9IHJlcXVpcmUoJy4vcm91dGVyJyksXG5cdENvbnRyb2xsZXIgPSByZXF1aXJlKCcuL2NvbnRyb2xsZXInKTtcblx0XG5cbnZhciBBcHAgPSBmdW5jdGlvbigpIHtcblx0dGhpcy5jb3JlID0gbmV3IE1hcmlvbmV0dGUuQXBwbGljYXRpb24oKTtcbn1cblxuQXBwLnByb3RvdHlwZS5zdGFydCA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcblx0dmFyIHRoYXQgPSB0aGlzO1xuXHRkYXRhUHJveHkuZmV0Y2goKTtcblx0ZGF0YVByb3h5Lm9uKCdkYXRhUmVhZHknLCBmdW5jdGlvbigpIHtcblx0XHR0aGF0LmNvcmUuc3RhcnQoKTtcblx0fSk7XG59XG5cbkFwcC5wcm90b3R5cGUuZGF0YVJlYWR5ID0gZnVuY3Rpb24oKSB7XG5cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBhcHAgPSBuZXcgQXBwKCk7XG5cbmFwcC5jb3JlLmFkZFJlZ2lvbnMoe1xuXHRyb290UmVnaW9uOiAnLnNpdGUnXG59KTtcblxuXG5hcHAuY29yZS5hZGRJbml0aWFsaXplcihmdW5jdGlvbihvcHRpb25zKSB7XG5cdC8vIGluaXRcbn0pO1xuXG5cbmFwcC5jb3JlLm9uKCdzdGFydCcsIGZ1bmN0aW9uKG9wdGlvbnMpIHtcblxuXHR0aGlzLmNvbnRyb2xsZXIgPSBuZXcgQ29udHJvbGxlcih0aGlzKTtcblx0dGhpcy5yb3V0ZXIgPSBuZXcgUm91dGVyKHtjb250cm9sbGVyOiB0aGlzLmNvbnRyb2xsZXJ9KTtcblxuXHRpZiAoQmFja2JvbmUuaGlzdG9yeSkge1xuXHRcdEJhY2tib25lLmhpc3Rvcnkuc3RhcnQoe3Jvb3Q6J2Rpc3QnfSk7XG5cdH1cblxufSk7IiwiXG52YXIgQ2F0ZWdvcnlNb2RlbCA9IEJhY2tib25lLk1vZGVsO1xuXG52YXIgQ2F0ZWdvcnlDb2xsZWN0aW9uID0gQmFja2JvbmUuQ29sbGVjdGlvbi5leHRlbmQoe1xuICAgIFxuICAgIG1vZGVsOiBDYXRlZ29yeU1vZGVsLFxuXG4gICAgdXJsOiBmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuICcvYXBpL3Rlcm0vY2F0ZWdvcnknO1xuICAgIH0sXG4gICAgXG4gICAgcGFyc2U6IGZ1bmN0aW9uKGRhdGEpIHtcbiAgICAgICAgdmFyIGZpbHRlcmVkID0gW107XG4gICAgICAgIF8uZWFjaChkYXRhLCBmdW5jdGlvbihpKSB7XG4gICAgICAgICAgICBpZiAoaS5jb3VudCA+IDApIHtcbiAgICAgICAgICAgICAgICBmaWx0ZXJlZC5wdXNoKGkpXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIGZpbHRlcmVkO1xuICAgIH1cblxufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gQ2F0ZWdvcnlDb2xsZWN0aW9uOyIsIlxudmFyIFBvc3RzID0gQmFja2JvbmUuQ29sbGVjdGlvbi5leHRlbmQoe1xuICAgIFxuICAgIHVybDogZnVuY3Rpb24oKSB7XG4gICAgICAgIC8vIHJldHVybiAnL2FwaS9wb3N0cyc7XG4gICAgICAgIHJldHVybiAnL3dwL3dwLWpzb24vcG9zdHMnO1xuICAgIH0sXG5cbiAgICBjYXRlZ29yeV9maWx0ZXI6IGZ1bmN0aW9uKGNhdGVnb3J5X2lkKSB7XG4gICAgICAgIHZhciBmaWx0ZXJlZCA9IF8uZmlsdGVyKHRoaXMudG9KU09OKCksIGZ1bmN0aW9uKG0pIHtcbiAgICAgICAgICAgIHZhciBmaWx0ZXIgPSBmYWxzZVxuICAgICAgICAgICAgXy5lYWNoKG0udGVybXMuY2F0ZWdvcnksIGZ1bmN0aW9uKGMpIHtcbiAgICAgICAgICAgICAgICBpZiAoYy5zbHVnID09PSBjYXRlZ29yeV9pZCkge1xuICAgICAgICAgICAgICAgICAgICBmaWx0ZXIgPSB0cnVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG5cbiAgICAgICAgICAgIHJldHVybiBmaWx0ZXI7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiBuZXcgQmFja2JvbmUuQ29sbGVjdGlvbiggZmlsdGVyZWQgKTtcbiAgICB9LFxuXG4gICAgd29yZHlfZmlsdGVyOiBmdW5jdGlvbih3b3JkeV9pZCkge1xuICAgICAgICB2YXIgZmlsdGVyZWQgPSBfLmZpbHRlcih0aGlzLnRvSlNPTigpLCBmdW5jdGlvbihtKSB7XG4gICAgICAgICAgICB2YXIgZmlsdGVyID0gZmFsc2VcbiAgICAgICAgICAgIF8uZWFjaChtLnRlcm1zLndvcmR5LCBmdW5jdGlvbihjKSB7XG4gICAgICAgICAgICAgICAgaWYgKGMuc2x1ZyA9PT0gd29yZHlfaWQpIHtcbiAgICAgICAgICAgICAgICAgICAgZmlsdGVyID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICByZXR1cm4gZmlsdGVyO1xuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gbmV3IEJhY2tib25lLkNvbGxlY3Rpb24oIGZpbHRlcmVkICk7XG4gICAgfVxuXG59KTtcblxubW9kdWxlLmV4cG9ydHMgPSBQb3N0czsiLCJcbnZhciBXb3JkeU1vZGVsID0gQmFja2JvbmUuTW9kZWw7XG5cbnZhciBXb3JkeUNvbGxlY3Rpb24gPSBCYWNrYm9uZS5Db2xsZWN0aW9uLmV4dGVuZCh7XG4gICAgXG4gICAgbW9kZWw6IFdvcmR5TW9kZWwsXG5cbiAgICB1cmw6IGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4gJy9hcGkvdGVybS93b3JkeSc7XG4gICAgfSxcblxuICAgIHBhcnNlOiBmdW5jdGlvbihkYXRhKSB7XG4gICAgICAgIHZhciBmaWx0ZXJlZCA9IFtdO1xuICAgICAgICBfLmVhY2goZGF0YSwgZnVuY3Rpb24oaSkge1xuICAgICAgICAgICAgaWYgKGkuY291bnQgPiAwKSB7XG4gICAgICAgICAgICAgICAgZmlsdGVyZWQucHVzaChpKVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiBmaWx0ZXJlZDtcbiAgICB9XG5cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IFdvcmR5Q29sbGVjdGlvbjsiLCJcbkxheW91dCA9IHJlcXVpcmUoJy4vdmlld3MvbWFpbkxheW91dFZpZXcnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBDb250cm9sbGVyID0gTWFyaW9uZXR0ZS5Db250cm9sbGVyLmV4dGVuZCh7XG4gICBcbiAgICBpbml0aWFsaXplOiBmdW5jdGlvbihhcHApIHtcbiAgICAgICAgdGhpcy5sYXlvdXQgPSBuZXcgTGF5b3V0KCk7XG4gICAgICAgIGFwcC5yb290UmVnaW9uLnNob3coIHRoaXMubGF5b3V0ICk7XG4gICAgfSxcblxuICAgIGhvbWU6IGZ1bmN0aW9uKCkge1xuICAgICAgICB0aGlzLmxheW91dC5ob21lKCk7XG4gICAgfSxcblxuICAgIHBvc3Q6IGZ1bmN0aW9uKGlkKSB7XG4gICAgICAgIHRoaXMubGF5b3V0LnBvc3QoaWQpXG4gICAgfSxcblxuICAgIGNhdGVnb3J5OiBmdW5jdGlvbihpZCkge1xuICAgICAgICB0aGlzLmxheW91dC5jYXRlZ29yeShpZCk7XG4gICAgfSxcblxuICAgIHdvcmR5OiBmdW5jdGlvbihpZCkge1xuICAgICAgICB0aGlzLmxheW91dC53b3JkaWVzKGlkKTsgIFxuICAgIH1cblxufSk7IiwiXG52YXIgUG9zdE1vZGVsICAgICAgICAgICAgID0gcmVxdWlyZSgnLi9tb2RlbC9wb3N0TW9kZWwnKSxcbiAgICBQb3N0c0NvbGxlY3Rpb24gICAgICAgPSByZXF1aXJlKCcuL2NvbGxlY3Rpb25zL3Bvc3RzQ29sbGVjdGlvbicpLFxuICAgIENhdGVnb3J5Q29sbGVjdGlvbiAgICA9IHJlcXVpcmUoJy4vY29sbGVjdGlvbnMvY2F0ZWdvcnlDb2xsZWN0aW9uJyksXG4gICAgV29yZHlDb2xsZWN0aW9uICAgICAgID0gcmVxdWlyZSgnLi9jb2xsZWN0aW9ucy93b3JkeUNvbGxlY3Rpb24nKSxcblxuICAgIHBvc3RNb2RlbCAgICAgICAgICAgICA9IG5ldyBQb3N0TW9kZWwoKSxcbiAgICBwb3N0c0NvbGxlY3Rpb24gICAgICAgPSBuZXcgUG9zdHNDb2xsZWN0aW9uKCksXG4gICAgY2F0ZWdvcnlDb2xsZWN0aW9uICAgID0gbmV3IENhdGVnb3J5Q29sbGVjdGlvbigpLFxuICAgIHdvcmR5Q29sbGVjdGlvbiAgICAgICA9IG5ldyBXb3JkeUNvbGxlY3Rpb24oKTtcblxuXG5tb2R1bGUuZXhwb3J0cyA9IF8uZXh0ZW5kKHtcblxuICAgIGNhdGVnb3JpZXNSZWFkeTogZmFsc2UsXG4gICAgd29yZGllc1JlYWR5OiBmYWxzZSxcbiAgICBwb3N0c1JlYWR5OiBmYWxzZSxcblxuICAgIGlzUmVhZHk6IGZ1bmN0aW9uKCkge1xuICAgICAgICBpZiAodGhpcy5jYXRlZ29yaWVzUmVhZHkgJiYgdGhpcy53b3JkaWVzUmVhZHkgJiYgdGhpcy5wb3N0c1JlYWR5KSB7XG4gICAgICAgICAgICB0aGlzLnRyaWdnZXIoJ2RhdGFSZWFkeScpO1xuICAgICAgICB9XG4gICAgfSxcblxuICAgIGZldGNoOiBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIHRoYXQgPSB0aGlzO1xuICAgICAgICBjYXRlZ29yeUNvbGxlY3Rpb24uZmV0Y2goe1xuICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgdGhhdC5jYXRlZ29yaWVzUmVhZHkgPSB0cnVlO1xuICAgICAgICAgICAgICAgIHRoYXQuaXNSZWFkeSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICB3b3JkeUNvbGxlY3Rpb24uZmV0Y2goe1xuICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgdGhhdC53b3JkaWVzUmVhZHkgPSB0cnVlO1xuICAgICAgICAgICAgICAgIHRoYXQuaXNSZWFkeSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICBwb3N0c0NvbGxlY3Rpb24uZmV0Y2goe1xuICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgdGhhdC5wb3N0c1JlYWR5ID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB0aGF0LmlzUmVhZHkoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfSxcblxuICAgIGdldENhdGVnb3JpZXM6IGZ1bmN0aW9uKHdoZXJlKSB7XG4gICAgICAgIGlmICh3aGVyZSkge1xuICAgICAgICAgICAgcmV0dXJuIG5ldyBCYWNrYm9uZS5Db2xsZWN0aW9uKGNhdGVnb3J5Q29sbGVjdGlvbi53aGVyZSh3aGVyZSkpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIGNhdGVnb3J5Q29sbGVjdGlvbjtcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBnZXRXb3JkaWVzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIHdvcmR5Q29sbGVjdGlvbjtcbiAgICB9LFxuXG4gICAgZ2V0UG9zdHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICB3aW5kb3cucG9zdHNDb2xsZWN0aW9uID0gcG9zdHNDb2xsZWN0aW9uO1xuICAgICAgICByZXR1cm4gcG9zdHNDb2xsZWN0aW9uO1xuICAgIH0sXG5cbiAgICBnZXRQb3N0c0J5Q2F0ZWdvcnk6IGZ1bmN0aW9uKGNhdGVnb3J5X2lkKSB7XG4gICAgICAgIHJldHVybiBwb3N0c0NvbGxlY3Rpb24uY2F0ZWdvcnlfZmlsdGVyKGNhdGVnb3J5X2lkKTtcbiAgICB9LFxuXG5cbiAgICBnZXRQb3N0c0J5V29yZGllczogZnVuY3Rpb24oY2F0ZWdvcnlfaWQpIHtcbiAgICAgICAgcmV0dXJuIHBvc3RzQ29sbGVjdGlvbi53b3JkeV9maWx0ZXIoY2F0ZWdvcnlfaWQpO1xuICAgIH0sXG5cbiAgICBnZXRQb3N0QnlJZDogZnVuY3Rpb24oaWQsIGNhbGxiYWNrKSB7XG4gICAgICAgIHBvc3RNb2RlbC5mZXRjaCh7XG4gICAgICAgICAgICB1cmw6ICcvd3Avd3AtanNvbi9wb3N0cy8nK2lkLFxuICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24obSkge1xuICAgICAgICAgICAgICAgIGNhbGxiYWNrKG0pXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgfVxuXG5cbn0sIEJhY2tib25lLkV2ZW50cyk7IiwiXG52YXIgYXBwID0gcmVxdWlyZSgnLi9hcHAnKTtcblxuYXBwLnN0YXJ0KCk7XG4iLCJcbm1vZHVsZS5leHBvcnRzID0gUG9zdE1vZGVsID0gQmFja2JvbmUuTW9kZWwuZXh0ZW5kKHtcblx0Ly8gdXJsOiBfQ09ORklHLmJhc2VfdXJsKycvYXBpL3Bvc3RzJ1xufSk7XG5cbiIsIlxubW9kdWxlLmV4cG9ydHMgPSBSb3V0ZXIgPSBNYXJpb25ldHRlLkFwcFJvdXRlci5leHRlbmQoe1xuICAgXG4gICAgYXBwUm91dGVzOiB7XG4gICAgICAgIFwiXCIgICAgICAgICAgICAgICAgICAgOiAgIFwiaG9tZVwiLFxuICAgICAgICBcInBvc3RzLzppZFwiICAgICAgICAgIDogICBcInBvc3RcIixcbiAgICAgICAgXCJwb3N0cy86aWQvOnNsdWdcIiAgICA6ICAgXCJwb3N0XCIsXG4gICAgICAgIFwiY2F0ZWdvcnkvOmlkXCIgICAgICAgOiAgIFwiY2F0ZWdvcnlcIixcbiAgICAgICAgXCJ3b3JkeS86aWRcIiAgICAgICAgICA6ICAgXCJ3b3JkeVwiXG4gICAgfVxuXG59KTsiLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKG9iail7XG52YXIgX190LF9fcD0nJyxfX2o9QXJyYXkucHJvdG90eXBlLmpvaW4scHJpbnQ9ZnVuY3Rpb24oKXtfX3ArPV9fai5jYWxsKGFyZ3VtZW50cywnJyk7fTtcbndpdGgob2JqfHx7fSl7XG5fX3ArPSdcXG48ZGl2IGNsYXNzPVwicGFnZS1oZWFkaW5nICcrXG4oKF9fdD0oc2x1ZykpPT1udWxsPycnOl9fdCkrXG4nXCI+XFxuICAgIDxkaXYgY2xhc3M9XCJpbm5lclwiPlxcbiAgICAgICAgPGgyPicrXG4oKF9fdD0obmFtZSkpPT1udWxsPycnOl9fdCkrXG4nPC9oMj5cXG4gICAgPC9kaXY+XFxuPC9kaXY+XFxuXFxuXFxuPGRpdiBjbGFzcz1cInBvc3RzIGNhdGVnb3J5LXBvc3RzXCI+XFxuICAgIFxcbiAgICA8ZGl2IGNsYXNzPVwibWFpbi1ibG9ja1wiPlxcblxcbiAgICAgICAgPGFzaWRlIGNsYXNzPVwicmlnaHQtc2lkZVwiPlxcblxcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0cmVuZGluZy1zdG9yaWVzIHdvcmRpZXMgJytcbigoX190PShzbHVnKSk9PW51bGw/Jyc6X190KStcbidcIj5cXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRvcGljICcrXG4oKF9fdD0oc2x1ZykpPT1udWxsPycnOl9fdCkrXG4nXCI+JytcbigoX190PShuYW1lKSk9PW51bGw/Jyc6X190KStcbic8L2Rpdj5cXG4gICAgICAgICAgICAgICAgPGgzIGNsYXNzPVwic2VjdGlvbi1oZWFkaW5nXCI+V09SRElFUzwvaDM+XFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsaW5rc1wiPlxcbiAgICAgICAgICAgICAgICAgICAgJztcbiBfLmVhY2god29yZGllcywgZnVuY3Rpb24oaSkgeyBcbl9fcCs9J1xcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsaW5rXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjd29yZHkvJytcbigoX190PShpLnNsdWcpKT09bnVsbD8nJzpfX3QpK1xuJ1wiPicrXG4oKF9fdD0oaS5uYW1lKSk9PW51bGw/Jyc6X190KStcbicgKCAnK1xuKChfX3Q9KGkuY291bnQpKT09bnVsbD8nJzpfX3QpK1xuJyApPC9hPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgICAgICAgICAgICAgJztcbiB9KTsgXG5fX3ArPSdcXG4gICAgICAgICAgICAgICAgICAgIDwhLS08ZGl2IGNsYXNzPVwibGlua1wiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjXCI+QWxsICggNiApPC9hPlxcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibGlua1wiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjXCI+WW91IE1heSBCZSBDb3VzaW5zICggNCApPC9hPlxcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibGlua1wiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjXCI+Q3JhdGUgRGlnZ2luZyAoIDIgKTwvYT5cXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2Pi0tPlxcbiAgICAgICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICA8L2Rpdj5cXG5cXG5cXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYWQyXCI+PC9kaXY+XFxuXFxuICAgICAgICA8L2FzaWRlPlxcblxcblxcblxcbiAgICAgICAgJztcbiBfLmVhY2goaXRlbXMsIGZ1bmN0aW9uKGkpIHsgXG5fX3ArPSdcXG4gICAgICAgIDxhcnRpY2xlIGNsYXNzPVwicG9zdCBpdGVtMVwiPlxcbiAgICAgICAgICAgICAgICBcXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibWVkaWFcIj5cXG4gICAgICAgICAgICAgICAgXFxuICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjcG9zdHMvJytcbigoX190PShpLklEKSk9PW51bGw/Jyc6X190KStcbicvJytcbigoX190PShpLnNsdWcpKT09bnVsbD8nJzpfX3QpK1xuJ1wiPlxcbiAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9XCInK1xuKChfX3Q9KGkuZmVhdHVyZWRfaW1hZ2Uuc291cmNlKSk9PW51bGw/Jyc6X190KStcbidcIiBhbHQ9XCJcIiB3aWR0aD1cIjEwMCVcIj5cXG4gICAgICAgICAgICAgICAgPC9hPlxcbiAgICAgICAgICAgIDwvZGl2PlxcblxcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtZXRhXCI+XFxuICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjJytcbigoX190PShpLmxpbmspKT09bnVsbD8nJzpfX3QpK1xuJ1wiIGNsYXNzPVwidGl0bGVcIj5cXG4gICAgICAgICAgICAgICAgICAgIDxoMT4nK1xuKChfX3Q9KGkudGl0bGUpKT09bnVsbD8nJzpfX3QpK1xuJzwvaDE+XFxuICAgICAgICAgICAgICAgIDwvYT5cXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dGhvci1kYXRlXCI+XFxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImF1dGhvclwiPkJ5IFxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjXCI+JytcbigoX190PShpLmF1dGhvci5kaXNwbGF5X25hbWUpKT09bnVsbD8nJzpfX3QpK1xuJzwvYT5cXG4gICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiZGF0ZVwiPm9uICcrXG4oKF9fdD0oaS5kYXRlKSk9PW51bGw/Jyc6X190KStcbic8L3NwYW4+XFxuICAgICAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZXhjZXJwdFwiPlxcbiAgICAgICAgICAgICAgICAgICAgPHA+JytcbigoX190PShpLmV4Y2VycHQpKT09bnVsbD8nJzpfX3QpK1xuJzwvcD5cXG4gICAgICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICA8L2FydGljbGU+XFxuICAgICAgICAnO1xuIH0pOyBcbl9fcCs9J1xcblxcblxcbiAgICA8L2Rpdj5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG48L2Rpdj5cXG5cXG48L2Rpdj5cXG5cXG4nO1xufVxucmV0dXJuIF9fcDtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKG9iail7XG52YXIgX190LF9fcD0nJyxfX2o9QXJyYXkucHJvdG90eXBlLmpvaW4scHJpbnQ9ZnVuY3Rpb24oKXtfX3ArPV9fai5jYWxsKGFyZ3VtZW50cywnJyk7fTtcbndpdGgob2JqfHx7fSl7XG5fX3ArPScgICAgICAgIDxkaXYgY2xhc3M9XCJpbm5lclwiPlxcbiAgICAgICAgICAgIFxcbiAgICAgICAgICAgIDx1bD5cXG4gICAgICAgICAgICAgICAgPGxpPjxhIGhyZWY9XCIjXCI+QUJPVVQ8L2E+PC9saT5cXG4gICAgICAgICAgICAgICAgPGxpPjxhIGhyZWY9XCIjXCI+Q09OVEFDVDwvYT48L2xpPlxcbiAgICAgICAgICAgICAgICA8bGk+PGEgaHJlZj1cIiNcIj48aW1nIHNyYz1cImltYWdlcy93b3JkeS1sb2dvLWxpZ2h0LnBuZ1wiIHdpZHRoPVwiMTAwXCI+PC9hPjwvbGk+XFxuICAgICAgICAgICAgICAgIDxsaT48YSBocmVmPVwiI1wiPlRFUk1TIE9GIFVTRTwvYT48L2xpPlxcbiAgICAgICAgICAgIDwvdWw+XFxuXFxuICAgICAgICA8L2Rpdj4nO1xufVxucmV0dXJuIF9fcDtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKG9iail7XG52YXIgX190LF9fcD0nJyxfX2o9QXJyYXkucHJvdG90eXBlLmpvaW4scHJpbnQ9ZnVuY3Rpb24oKXtfX3ArPV9fai5jYWxsKGFyZ3VtZW50cywnJyk7fTtcbndpdGgob2JqfHx7fSl7XG5fX3ArPSdcXG4gICAgJztcbiBpZiAoZmVhdHVyZWRfaW1hZ2UpIHsgXG5fX3ArPSdcXG4gICAgPGRpdiBjbGFzcz1cIm1lZGlhXCI+XFxuICAgICAgICAnO1xuIGlmICh0ZXJtcy5jYXRlZ29yeSkgeyBcbl9fcCs9J1xcbiAgICAgICAgPGRpdiBjbGFzcz1cImNhdGVnb3J5XCI+XFxuICAgICAgICAgICAgPGEgaHJlZj1cIiNjYXRlZ29yeS8nK1xuKChfX3Q9KHRlcm1zLmNhdGVnb3J5WzBdLnNsdWcpKT09bnVsbD8nJzpfX3QpK1xuJ1wiIGNsYXNzPVwiJytcbigoX190PSh0ZXJtcy5jYXRlZ29yeVswXS5zbHVnKSk9PW51bGw/Jyc6X190KStcbidcIj4nK1xuKChfX3Q9KHRlcm1zLmNhdGVnb3J5WzBdLm5hbWUpKT09bnVsbD8nJzpfX3QpK1xuJzwvYT5cXG4gICAgICAgIDwvZGl2PlxcbiAgICAgICAgJztcbiB9IFxuX19wKz0nXFxuICAgICAgICA8YSBocmVmPVwiI3Bvc3RzLycrXG4oKF9fdD0oSUQpKT09bnVsbD8nJzpfX3QpK1xuJy8nK1xuKChfX3Q9KHNsdWcpKT09bnVsbD8nJzpfX3QpK1xuJ1wiPlxcbiAgICAgICAgICAgIDxpbWcgc3JjPVwiJytcbigoX190PShmZWF0dXJlZF9pbWFnZS5zb3VyY2UpKT09bnVsbD8nJzpfX3QpK1xuJ1wiIGFsdD1cIlwiIHdpZHRoPVwiMTAwJVwiPjwvYT5cXG4gICAgPC9kaXY+XFxuICAgICc7XG4gfSBcbl9fcCs9J1xcblxcbiAgICA8ZGl2IGNsYXNzPVwibWV0YVwiPlxcbiAgICAgICAgPGEgaHJlZj1cIiNwb3N0cy8nK1xuKChfX3Q9KElEKSk9PW51bGw/Jyc6X190KStcbicvJytcbigoX190PShzbHVnKSk9PW51bGw/Jyc6X190KStcbidcIiBjbGFzcz1cInRpdGxlXCI+XFxuICAgICAgICAgICAgPGgxPicrXG4oKF9fdD0odGl0bGUpKT09bnVsbD8nJzpfX3QpK1xuJzwvaDE+PC9hPlxcbiAgICAgICAgPGRpdiBjbGFzcz1cImF1dGhvci1kYXRlXCI+XFxuICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJhdXRob3JcIj5CeSA8YSBocmVmPVwiI1wiPicrXG4oKF9fdD0oYXV0aG9yLm5hbWUpKT09bnVsbD8nJzpfX3QpK1xuJzwvYT48L3NwYW4+XFxuICAgICAgICAgICAgJztcbiB2YXIgZGF0ZSA9IG5ldyBEYXRlKGRhdGUpOyBcbl9fcCs9J1xcbiAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiZGF0ZVwiPm9uICcrXG4oKF9fdD0oZGF0ZSkpPT1udWxsPycnOl9fdCkrXG4nPC9zcGFuPlxcbiAgICAgICAgPC9kaXY+XFxuICAgICAgICA8ZGl2IGNsYXNzPVwiZXhjZXJwdFwiPlxcbiAgICAgICAgICAgIDxwPicrXG4oKF9fdD0oZXhjZXJwdCkpPT1udWxsPycnOl9fdCkrXG4nPC9wPlxcbiAgICAgICAgPC9kaXY+XFxuICAgIDwvZGl2PlxcbiAgICA8YSBocmVmPVwiI3Bvc3RzLycrXG4oKF9fdD0oSUQpKT09bnVsbD8nJzpfX3QpK1xuJy8nK1xuKChfX3Q9KHNsdWcpKT09bnVsbD8nJzpfX3QpK1xuJ1wiIGNsYXNzPVwibW9yZVwiPlJlYWQgTW9yZTwvYT4nO1xufVxucmV0dXJuIF9fcDtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKG9iail7XG52YXIgX190LF9fcD0nJyxfX2o9QXJyYXkucHJvdG90eXBlLmpvaW4scHJpbnQ9ZnVuY3Rpb24oKXtfX3ArPV9fai5jYWxsKGFyZ3VtZW50cywnJyk7fTtcbndpdGgob2JqfHx7fSl7XG5fX3ArPScgICAgICAgIDxkaXYgY2xhc3M9XCJmZWF0dXJlZCBtZW51LWFuaW1cIj5cXG4gICAgICAgICAgICBcXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaGVyby1zZWN0aW9uXCI+XFxuXFxuICAgICAgICAgICAgICAgICc7XG4gXy5lYWNoKGZlYXR1cmVkLCBmdW5jdGlvbihpLCBpbmRleCkgeyBcbl9fcCs9J1xcblxcbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiI3Bvc3RzLycrXG4oKF9fdD0oaS5nZXQoJ0lEJykpKT09bnVsbD8nJzpfX3QpK1xuJy8nK1xuKChfX3Q9KGkuZ2V0KCdzbHVnJykpKT09bnVsbD8nJzpfX3QpK1xuJ1wiIGNsYXNzPVwiaXRlbSBpdGVtJytcbigoX190PShpbmRleCkpPT1udWxsPycnOl9fdCkrXG4nXCIgc3R5bGU9XCJiYWNrZ3JvdW5kLWltYWdlOnVybCgnK1xuKChfX3Q9KGkuZ2V0KCdmZWF0dXJlZF9pbWFnZScpLnNvdXJjZSkpPT1udWxsPycnOl9fdCkrXG4nKVwiPlxcblxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbnRlbnRcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8aDE+JytcbigoX190PShpLmdldCgndGl0bGUnKSkpPT1udWxsPycnOl9fdCkrXG4nPC9oMT5cXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgICAgICAgICA8L2E+XFxuXFxuICAgICAgICAgICAgICAgICc7XG4gfSk7IFxuX19wKz0nXFxuXFxuICAgICAgICAgICAgPC9kaXY+XFxuXFxuXFxuICAgICAgICA8L2Rpdj5cXG5cXG4gICAgICAgIDwhLS08ZGl2IGNsYXNzPVwibWFpbi1uYXYgbmF2MiBtZW51LWFuaW1cIj5cXG4gICAgICAgICAgICA8bmF2PlxcbiAgICAgICAgICAgIDx1bD5cXG4gICAgICAgICAgICAgICAgPGxpPjxhIGhyZWY9XCJjb21pY3MuaHRtbFwiIGNsYXNzPVwiY29taWNzXCI+Q29taWNzPC9hPjwvbGk+XFxuICAgICAgICAgICAgICAgICc7XG4gXy5lYWNoKGNhdGVnb3JpZXMsIGZ1bmN0aW9uKGkpIHsgXG5fX3ArPSdcXG4gICAgICAgICAgICAgICAgPGxpPjxhIGhyZWY9XCInK1xuKChfX3Q9KGkuc2x1ZykpPT1udWxsPycnOl9fdCkrXG4nXCIgY2xhc3M9XCInK1xuKChfX3Q9KGkuc2x1ZykpPT1udWxsPycnOl9fdCkrXG4nXCI+JytcbigoX190PShpLm5hbWUpKT09bnVsbD8nJzpfX3QpK1xuJzwvYT48L2xpPlxcbiAgICAgICAgICAgICAgICAnO1xuIH0pOyBcbl9fcCs9J1xcbiAgICAgICAgICAgIDwvdWw+XFxuICAgICAgICAgICAgPC9uYXY+XFxuICAgICAgICA8L2Rpdj4tLT4nO1xufVxucmV0dXJuIF9fcDtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKG9iail7XG52YXIgX190LF9fcD0nJyxfX2o9QXJyYXkucHJvdG90eXBlLmpvaW4scHJpbnQ9ZnVuY3Rpb24oKXtfX3ArPV9fai5jYWxsKGFyZ3VtZW50cywnJyk7fTtcbndpdGgob2JqfHx7fSl7XG5fX3ArPScgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicG9zdHNcIj5cXG4gICAgICAgICAgICAgICAgXFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtYWluLWJsb2NrXCI+XFxuXFxuICAgICAgICAgICAgICAgICAgICAnO1xuIF8uZWFjaChfLnJhbmdlKDMsNCksIGZ1bmN0aW9uKGkpIHsgXG5fX3ArPSdcXG4gICAgICAgICAgICAgICAgICAgIDxhcnRpY2xlIGNsYXNzPVwicG9zdCBpdGVtMVwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICc7XG4gaWYgKGl0ZW1zW2ldLmZlYXR1cmVkX2ltYWdlKSB7IFxuX19wKz0nXFxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1lZGlhXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICc7XG4gaWYgKGl0ZW1zW2ldLnRlcm1zLmNhdGVnb3J5KSB7IFxuX19wKz0nXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjYXRlZ29yeVwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cIiNjYXRlZ29yeS8nK1xuKChfX3Q9KGl0ZW1zW2ldLnRlcm1zLmNhdGVnb3J5WzBdLnNsdWcpKT09bnVsbD8nJzpfX3QpK1xuJ1wiIGNsYXNzPVwiJytcbigoX190PShpdGVtc1tpXS50ZXJtcy5jYXRlZ29yeVswXS5zbHVnKSk9PW51bGw/Jyc6X190KStcbidcIj4nK1xuKChfX3Q9KGl0ZW1zW2ldLnRlcm1zLmNhdGVnb3J5WzBdLm5hbWUpKT09bnVsbD8nJzpfX3QpK1xuJzwvYT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICc7XG4gfSBcbl9fcCs9J1xcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiI3Bvc3RzLycrXG4oKF9fdD0oaXRlbXNbaV0uSUQpKT09bnVsbD8nJzpfX3QpK1xuJy8nK1xuKChfX3Q9KGl0ZW1zW2ldLnNsdWcpKT09bnVsbD8nJzpfX3QpK1xuJ1wiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9XCInK1xuKChfX3Q9KGl0ZW1zW2ldLmZlYXR1cmVkX2ltYWdlLnNvdXJjZSkpPT1udWxsPycnOl9fdCkrXG4nXCIgYWx0PVwiXCIgd2lkdGg9XCIxMDAlXCI+PC9hPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICc7XG4gfSBcbl9fcCs9J1xcblxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtZXRhXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjcG9zdHMvJytcbigoX190PShpdGVtc1tpXS5JRCkpPT1udWxsPycnOl9fdCkrXG4nLycrXG4oKF9fdD0oaXRlbXNbaV0uc2x1ZykpPT1udWxsPycnOl9fdCkrXG4nXCIgY2xhc3M9XCJ0aXRsZVwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGgxPicrXG4oKF9fdD0oaXRlbXNbaV0udGl0bGUpKT09bnVsbD8nJzpfX3QpK1xuJzwvaDE+PC9hPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0aG9yLWRhdGVcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYXV0aG9yXCI+QnkgPGEgaHJlZj1cIiNcIj4nK1xuKChfX3Q9KGl0ZW1zW2ldLmF1dGhvci5uYW1lKSk9PW51bGw/Jyc6X190KStcbic8L2E+PC9zcGFuPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJztcbiB2YXIgZGF0ZSA9IG5ldyBEYXRlKGl0ZW1zW2ldLmRhdGUpOyBcbl9fcCs9J1xcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJkYXRlXCI+b24gJytcbigoX190PShkYXRlKSk9PW51bGw/Jyc6X190KStcbic8L3NwYW4+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZXhjZXJwdFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHA+JytcbigoX190PShpdGVtc1tpXS5leGNlcnB0KSk9PW51bGw/Jyc6X190KStcbic8L3A+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjcG9zdHMvJytcbigoX190PShpdGVtc1tpXS5JRCkpPT1udWxsPycnOl9fdCkrXG4nLycrXG4oKF9fdD0oaXRlbXNbaV0uc2x1ZykpPT1udWxsPycnOl9fdCkrXG4nXCIgY2xhc3M9XCJtb3JlXCI+UmVhZCBNb3JlPC9hPlxcbiAgICAgICAgICAgICAgICAgICAgPC9hcnRpY2xlPlxcbiAgICAgICAgICAgICAgICAgICAgJztcbiB9KTsgXG5fX3ArPSdcXG5cXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0cmVuZGluZy1zdG9yaWVzXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPGgzIGNsYXNzPVwic2VjdGlvbi1oZWFkaW5nXCI+VFJFTkRJTkcgU1RPUklFUzwvaDM+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxpbmtzXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICc7XG4gXy5lYWNoKF8ucmFuZ2UoNSwxMCksIGZ1bmN0aW9uKGkpIHsgXG5fX3ArPSdcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxpbmtcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjXCI+JytcbigoX190PShpdGVtc1tpXS50aXRsZSkpPT1udWxsPycnOl9fdCkrXG4nPC9hPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJztcbiB9KTsgXG5fX3ArPSdcXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcblxcblxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImFkMlwiPjwvZGl2PlxcblxcblxcbiAgICAgICAgICAgICAgICA8L2Rpdj5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxcblxcbiAgICAgICAgICAgICAgICAgICAgJztcbiBfLmVhY2goXy5yYW5nZSg0LDYpLCBmdW5jdGlvbihpKSB7IFxuX19wKz0nXFxuICAgICAgICAgICAgICAgICAgICA8YXJ0aWNsZSBjbGFzcz1cInBvc3QgaXRlbTNcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAnO1xuIGlmIChpdGVtc1tpXS5mZWF0dXJlZF9pbWFnZSkgeyBcbl9fcCs9J1xcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtZWRpYVwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnO1xuIGlmIChpdGVtc1tpXS50ZXJtcy5jYXRlZ29yeSkgeyBcbl9fcCs9J1xcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2F0ZWdvcnlcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjY2F0ZWdvcnkvJytcbigoX190PShpdGVtc1tpXS50ZXJtcy5jYXRlZ29yeVswXS5zbHVnKSk9PW51bGw/Jyc6X190KStcbidcIiBjbGFzcz1cIicrXG4oKF9fdD0oaXRlbXNbaV0udGVybXMuY2F0ZWdvcnlbMF0uc2x1ZykpPT1udWxsPycnOl9fdCkrXG4nXCI+JytcbigoX190PShpdGVtc1tpXS50ZXJtcy5jYXRlZ29yeVswXS5uYW1lKSk9PW51bGw/Jyc6X190KStcbic8L2E+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnO1xuIH0gXG5fX3ArPSdcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cIiNwb3N0cy8nK1xuKChfX3Q9KGl0ZW1zW2ldLklEKSk9PW51bGw/Jyc6X190KStcbicvJytcbigoX190PShpdGVtc1tpXS5zbHVnKSk9PW51bGw/Jyc6X190KStcbidcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPVwiJytcbigoX190PShpdGVtc1tpXS5mZWF0dXJlZF9pbWFnZS5zb3VyY2UpKT09bnVsbD8nJzpfX3QpK1xuJ1wiIGFsdD1cIlwiIHdpZHRoPVwiMTAwJVwiPjwvYT5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAnO1xuIH0gXG5fX3ArPSdcXG5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibWV0YVwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiI3Bvc3RzLycrXG4oKF9fdD0oaXRlbXNbaV0uSUQpKT09bnVsbD8nJzpfX3QpK1xuJy8nK1xuKChfX3Q9KGl0ZW1zW2ldLnNsdWcpKT09bnVsbD8nJzpfX3QpK1xuJ1wiIGNsYXNzPVwidGl0bGVcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoMT4nK1xuKChfX3Q9KGl0ZW1zW2ldLnRpdGxlKSk9PW51bGw/Jyc6X190KStcbic8L2gxPjwvYT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dGhvci1kYXRlXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImF1dGhvclwiPkJ5IDxhIGhyZWY9XCIjXCI+JytcbigoX190PShpdGVtc1tpXS5hdXRob3IubmFtZSkpPT1udWxsPycnOl9fdCkrXG4nPC9hPjwvc3Bhbj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICc7XG4gdmFyIGRhdGUgPSBuZXcgRGF0ZShpdGVtc1tpXS5kYXRlKTsgXG5fX3ArPSdcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiZGF0ZVwiPm9uICcrXG4oKF9fdD0oZGF0ZSkpPT1udWxsPycnOl9fdCkrXG4nPC9zcGFuPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImV4Y2VycHRcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwPicrXG4oKF9fdD0oaXRlbXNbaV0uZXhjZXJwdCkpPT1udWxsPycnOl9fdCkrXG4nPC9wPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiI3Bvc3RzLycrXG4oKF9fdD0oaXRlbXNbaV0uSUQpKT09bnVsbD8nJzpfX3QpK1xuJy8nK1xuKChfX3Q9KGl0ZW1zW2ldLnNsdWcpKT09bnVsbD8nJzpfX3QpK1xuJ1wiIGNsYXNzPVwibW9yZVwiPlJlYWQgTW9yZTwvYT5cXG4gICAgICAgICAgICAgICAgICAgIDwvYXJ0aWNsZT5cXG4gICAgICAgICAgICAgICAgICAgICc7XG4gfSk7IFxuX19wKz0nXFxuXFxuICAgICAgICAgICAgICAgIDwvZGl2PlxcblxcblxcblxcblxcblxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XFxuXFxuICAgICAgICAgICAgICAgICAgICAnO1xuIF8uZWFjaChfLnJhbmdlKDYsMTIpLCBmdW5jdGlvbihpKSB7IFxuX19wKz0nXFxuICAgICAgICAgICAgICAgICAgICA8YXJ0aWNsZSBjbGFzcz1cInBvc3QgaXRlbTJcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAnO1xuIGlmIChpdGVtc1tpXS5mZWF0dXJlZF9pbWFnZSkgeyBcbl9fcCs9J1xcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtZWRpYVwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnO1xuIGlmIChpdGVtc1tpXS50ZXJtcy5jYXRlZ29yeSkgeyBcbl9fcCs9J1xcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2F0ZWdvcnlcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjY2F0ZWdvcnkvJytcbigoX190PShpdGVtc1tpXS50ZXJtcy5jYXRlZ29yeVswXS5zbHVnKSk9PW51bGw/Jyc6X190KStcbidcIiBjbGFzcz1cIicrXG4oKF9fdD0oaXRlbXNbaV0udGVybXMuY2F0ZWdvcnlbMF0uc2x1ZykpPT1udWxsPycnOl9fdCkrXG4nXCI+JytcbigoX190PShpdGVtc1tpXS50ZXJtcy5jYXRlZ29yeVswXS5uYW1lKSk9PW51bGw/Jyc6X190KStcbic8L2E+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnO1xuIH0gXG5fX3ArPSdcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cIiNwb3N0cy8nK1xuKChfX3Q9KGl0ZW1zW2ldLklEKSk9PW51bGw/Jyc6X190KStcbicvJytcbigoX190PShpdGVtc1tpXS5zbHVnKSk9PW51bGw/Jyc6X190KStcbidcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPVwiJytcbigoX190PShpdGVtc1tpXS5mZWF0dXJlZF9pbWFnZS5zb3VyY2UpKT09bnVsbD8nJzpfX3QpK1xuJ1wiIGFsdD1cIlwiIHdpZHRoPVwiMTAwJVwiPjwvYT5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAnO1xuIH0gXG5fX3ArPSdcXG5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibWV0YVwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiI3Bvc3RzLycrXG4oKF9fdD0oaXRlbXNbaV0uSUQpKT09bnVsbD8nJzpfX3QpK1xuJy8nK1xuKChfX3Q9KGl0ZW1zW2ldLnNsdWcpKT09bnVsbD8nJzpfX3QpK1xuJ1wiIGNsYXNzPVwidGl0bGVcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoMT4nK1xuKChfX3Q9KGl0ZW1zW2ldLnRpdGxlKSk9PW51bGw/Jyc6X190KStcbic8L2gxPjwvYT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dGhvci1kYXRlXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImF1dGhvclwiPkJ5IDxhIGhyZWY9XCIjXCI+JytcbigoX190PShpdGVtc1tpXS5hdXRob3IubmFtZSkpPT1udWxsPycnOl9fdCkrXG4nPC9hPjwvc3Bhbj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICc7XG4gdmFyIGRhdGUgPSBuZXcgRGF0ZShpdGVtc1tpXS5kYXRlKTsgXG5fX3ArPSdcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiZGF0ZVwiPm9uICcrXG4oKF9fdD0oZGF0ZSkpPT1udWxsPycnOl9fdCkrXG4nPC9zcGFuPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImV4Y2VycHRcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwPicrXG4oKF9fdD0oaXRlbXNbaV0uZXhjZXJwdCkpPT1udWxsPycnOl9fdCkrXG4nPC9wPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiI3Bvc3RzLycrXG4oKF9fdD0oaXRlbXNbaV0uSUQpKT09bnVsbD8nJzpfX3QpK1xuJy8nK1xuKChfX3Q9KGl0ZW1zW2ldLnNsdWcpKT09bnVsbD8nJzpfX3QpK1xuJ1wiIGNsYXNzPVwibW9yZVwiPlJlYWQgTW9yZTwvYT5cXG4gICAgICAgICAgICAgICAgICAgIDwvYXJ0aWNsZT5cXG4gICAgICAgICAgICAgICAgICAgICc7XG4gfSk7IFxuX19wKz0nXFxuXFxuICAgICAgICAgICAgICAgIDwvZGl2PlxcblxcblxcblxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9yZS1wb3N0c1wiPlxcblxcbiAgICAgICAgICAgICAgICA8L2Rpdj5cXG5cXG5cXG5cXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImxvYWQtbW9yZVwiPkxPQUQgTU9SRTwvYnV0dG9uPlxcblxcblxcblxcbiAgICAgICAgICAgIDwvZGl2Pic7XG59XG5yZXR1cm4gX19wO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24ob2JqKXtcbnZhciBfX3QsX19wPScnLF9faj1BcnJheS5wcm90b3R5cGUuam9pbixwcmludD1mdW5jdGlvbigpe19fcCs9X19qLmNhbGwoYXJndW1lbnRzLCcnKTt9O1xud2l0aChvYmp8fHt9KXtcbl9fcCs9J1xcbiAgICAgICAgPGRpdiBjbGFzcz1cInRvcG5hdlwiPlxcblxcbiAgICAgICAgPC9kaXY+XFxuXFxuICAgICAgICA8ZGl2IGNsYXNzPVwic2lkZS1tZW51IG1lbnUtYW5pbVwiPlxcblxcbiAgICAgICAgPC9kaXY+XFxuICAgICAgICA8ZGl2IGNsYXNzPVwibWVudS1vdmVybGF5XCI+PC9kaXY+XFxuXFxuXFxuICAgICAgICA8ZGl2IGNsYXNzPVwiaG9tZS1jb250ZW50XCI+XFxuXFxuICAgICAgICA8L2Rpdj5cXG5cXG4gICAgICAgIDxkaXYgY2xhc3M9XCJhZFwiPjwvZGl2PlxcblxcbiAgICAgICAgPGRpdiBjbGFzcz1cIm1haW4tY29udGVudFwiPlxcblxcblxcbiAgICAgICAgPC9kaXY+XFxuXFxuXFxuICAgICAgICA8ZGl2IGNsYXNzPVwibWFpbi1mb290ZXJcIj5cXG4gICAgICAgICAgICBcXG4gICAgICAgIDwvZGl2Pic7XG59XG5yZXR1cm4gX19wO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24ob2JqKXtcbnZhciBfX3QsX19wPScnLF9faj1BcnJheS5wcm90b3R5cGUuam9pbixwcmludD1mdW5jdGlvbigpe19fcCs9X19qLmNhbGwoYXJndW1lbnRzLCcnKTt9O1xud2l0aChvYmp8fHt9KXtcbl9fcCs9J1xcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJwb3N0c1wiPlxcbiAgICAgICAgICAgICAgICBcXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1haW4tYmxvY2tcIj5cXG5cXG4gICAgICAgICAgICAgICAgICAgIDxhcnRpY2xlIGNsYXNzPVwicG9zdCBpdGVtMVwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIFxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxoMT4nK1xuKChfX3Q9KHRpdGxlKSk9PW51bGw/Jyc6X190KStcbic8L2gxPlxcblxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtZXRhXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0aG9yLWRhdGVcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYXV0aG9yXCI+QnkgPGEgaHJlZj1cIiNcIj4nK1xuKChfX3Q9KGF1dGhvci5uYW1lKSk9PW51bGw/Jyc6X190KStcbic8L2E+PC9zcGFuPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJkYXRlXCI+b24gJytcbigoX190PShkYXRlKSk9PW51bGw/Jyc6X190KStcbic8L3NwYW4+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29udGVudFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJytcbigoX190PShjb250ZW50KSk9PW51bGw/Jyc6X190KStcbidcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICAgICAgICAgICAgICA8L2FydGljbGU+XFxuXFxuXFxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidHJlbmRpbmctc3Rvcmllc1wiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxoMyBjbGFzcz1cInNlY3Rpb24taGVhZGluZ1wiPlJFTEFURUQgU1RPUklFUzwvaDM+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxpbmtzXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICc7XG4gXy5lYWNoKHJlbGF0ZWQsIGZ1bmN0aW9uKGkpIHsgXG5fX3ArPSdcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxpbmtcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjcG9zdHMvJytcbigoX190PShpLklEKSk9PW51bGw/Jyc6X190KStcbicvJytcbigoX190PShpLnNsdWcpKT09bnVsbD8nJzpfX3QpK1xuJ1wiPicrXG4oKF9fdD0oaS50aXRsZSkpPT1udWxsPycnOl9fdCkrXG4nPC9hPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJztcbiB9KTsgXG5fX3ArPSdcXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcblxcbiAgICAgICAgICAgICAgICA8L2Rpdj5cXG5cXG5cXG4gICAgICAgICAgICA8L2Rpdj4nO1xufVxucmV0dXJuIF9fcDtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKG9iail7XG52YXIgX190LF9fcD0nJyxfX2o9QXJyYXkucHJvdG90eXBlLmpvaW4scHJpbnQ9ZnVuY3Rpb24oKXtfX3ArPV9fai5jYWxsKGFyZ3VtZW50cywnJyk7fTtcbndpdGgob2JqfHx7fSl7XG5fX3ArPSdcXG4gICAgICAgIDxkaXYgY2xhc3M9XCJsb2dvXCI+XFxuICAgICAgICAgICAgPGltZyBzcmM9XCJpbWFnZXMvd29yZHktbG9nby5wbmdcIiBhbHQ9XCJcIj5cXG4gICAgICAgIDwvZGl2PlxcblxcbiAgICAgICAgPGRpdiBjbGFzcz1cIndvcmRpZXNcIj5cXG4gICAgICAgICAgICA8aDM+V09SRElFUzwvaDM+XFxuICAgICAgICAgICAgPHVsPlxcbiAgICAgICAgICAgICAgICAnO1xuIF8uZWFjaCh3b3JkaWVzLCBmdW5jdGlvbihpKSB7IFxuX19wKz0nXFxuICAgICAgICAgICAgICAgIDxsaT48YSBocmVmPVwiI3dvcmR5LycrXG4oKF9fdD0oaS5zbHVnKSk9PW51bGw/Jyc6X190KStcbidcIj4nK1xuKChfX3Q9KGkubmFtZSkpPT1udWxsPycnOl9fdCkrXG4nPC9hPjwvbGk+XFxuICAgICAgICAgICAgICAgICc7XG4gfSk7IFxuX19wKz0nXFxuICAgICAgICAgICAgPC91bD5cXG4gICAgICAgIDwvZGl2PlxcblxcbiAgICAgICAgPGRpdiBjbGFzcz1cIm1haW5cIj5cXG4gICAgICAgICAgICA8aDM+Q0FURUdPUklFUzwvaDM+XFxuICAgICAgICAgICAgPHVsPlxcbiAgICAgICAgICAgICAgICAnO1xuIF8uZWFjaChjYXRlZ29yaWVzLCBmdW5jdGlvbihpKSB7IFxuX19wKz0nXFxuICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIicrXG4oKF9fdD0oaS5zbHVnKSk9PW51bGw/Jyc6X190KStcbidcIj48YSBocmVmPVwiI2NhdGVnb3J5LycrXG4oKF9fdD0oaS5zbHVnKSk9PW51bGw/Jyc6X190KStcbidcIj4nK1xuKChfX3Q9KGkubmFtZSkpPT1udWxsPycnOl9fdCkrXG4nPC9hPjwvbGk+XFxuICAgICAgICAgICAgICAgICc7XG4gfSk7IFxuX19wKz0nXFxuICAgICAgICAgICAgPC91bD5cXG4gICAgICAgIDwvZGl2Plxcbic7XG59XG5yZXR1cm4gX19wO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24ob2JqKXtcbnZhciBfX3QsX19wPScnLF9faj1BcnJheS5wcm90b3R5cGUuam9pbixwcmludD1mdW5jdGlvbigpe19fcCs9X19qLmNhbGwoYXJndW1lbnRzLCcnKTt9O1xud2l0aChvYmp8fHt9KXtcbl9fcCs9J1xcbiAgICAgICAgPGRpdiBjbGFzcz1cIm1lbnUtaWNvbiBtZW51LWFuaW1cIj48c3Bhbj48L3NwYW4+PC9kaXY+XFxuXFxuICAgICAgICA8YSBocmVmPVwiaW5kZXguaHRtbFwiIGNsYXNzPVwidGV4dC1sb2dvXCI+V09SRFkgU0FOQ0hFWjwvYT5cXG5cXG4gICAgICAgIDxkaXYgY2xhc3M9XCJtYWluLW5hdiBuYXYxIG1lbnUtYW5pbSBvblwiPlxcbiAgICAgICAgICAgIDxuYXY+XFxuICAgICAgICAgICAgICAgIDx1bD5cXG4gICAgICAgICAgICAgICAgICAgICc7XG4gXy5lYWNoKG1lbnUsIGZ1bmN0aW9uKGkpIHsgXG5fX3ArPSdcXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCInK1xuKChfX3Q9KGkuc2x1ZykpPT1udWxsPycnOl9fdCkrXG4nXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCInK1xuKChfX3Q9KGkuc2x1ZykpPT1udWxsPycnOl9fdCkrXG4nXCIgY2xhc3M9XCInK1xuKChfX3Q9KGkuc2x1ZykpPT1udWxsPycnOl9fdCkrXG4nXCI+JytcbigoX190PShpLm5hbWUpKT09bnVsbD8nJzpfX3QpK1xuJzwvYT5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxcbiAgICAgICAgICAgICAgICAgICAgJztcbiB9KSBcbl9fcCs9J1xcbiAgICBcXG4gICAgICAgICAgICAgICAgICAgICAgICA8IS0tIDxkaXYgY2xhc3M9XCJkZC1tZW51XCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48YSBocmVmPVwiY29zcGxheVwiIGNsYXNzPVwiY29zcGxheVwiPkNvc3BsYXk8L2E+PC9saT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48YSBocmVmPVwiYW5pbWVcIiBjbGFzcz1cImFuaW1lXCI+QW5pbWU8L2E+PC9saT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48YSBocmVmPVwiZ2FkZ2V0c1wiIGNsYXNzPVwiZ2FkZ2V0c1wiPkdhZGdldHM8L2E+PC9saT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48YSBocmVmPVwic2NyZWVuaW5nc1wiIGNsYXNzPVwic2NyZWVuaW5nc1wiPlNjcmVlbmluZ3M8L2E+PC9saT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48YSBocmVmPVwiZmVhdHVyZXNcIiBjbGFzcz1cImZlYXR1cmVzXCI+RmVhdHVyZXM8L2E+PC9saT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gLS0+XFxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxcbiAgICAgICAgICAgICAgICA8L3VsPlxcbiAgICAgICAgICAgIDwvbmF2PlxcbiAgICAgICAgPC9kaXY+XFxuXFxuICAgICAgICA8ZGl2IGNsYXNzPVwidG9wLXJpZ2h0XCI+XFxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInNvY2lhbFwiPlxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNvbiBpY29uLWZhY2Vib29rXCI+PC9kaXY+XFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpY29uIGljb24tdHdpdHRlclwiPjwvZGl2PlxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNvbiBpY29uLWluc3RhZ3JhbVwiPjwvZGl2PlxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNvbiBpY29uLXR1bWJsclwiPjwvZGl2PlxcbiAgICAgICAgICAgIDwvZGl2PlxcblxcbiAgICAgICAgICAgIDwhLS08ZGl2IGNsYXNzPVwic2VhcmNoLWJveFwiPlxcbiAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBpZD1cInNlYXJjaFwiIHBsYWNlaG9sZGVyPVwiU0VBUkNIXCI+XFxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaWNvbi1zZWFyY2hcIj48L3NwYW4+XFxuICAgICAgICAgICAgPC9kaXY+LS0+XFxuICAgICAgICA8L2Rpdj5cXG5cXG4nO1xufVxucmV0dXJuIF9fcDtcbn07XG4iLCJcbnZhciB0ZW1wbGF0ZSA9IHJlcXVpcmUoJy4uLy4uL3RlbXBsYXRlcy9jYXRlZ29yeS9sYXlvdXQuaHRtbCcpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IE1hcmlvbmV0dGUuTGF5b3V0Vmlldy5leHRlbmQoe1xuICAgIHRlbXBsYXRlOiB0ZW1wbGF0ZSxcblxuICAgIG9uU2hvdzogZnVuY3Rpb24oKSB7XG4gICAgICAgICQod2luZG93KS5vbignc2Nyb2xsJywgdGhpcy5vblNjcm9sbCk7XG4gICAgICAgIHRoaXMub25TY3JvbGwoKTtcblxuICAgICAgICAkKHdpbmRvdykub24oJ3Jlc2l6ZScsIHRoaXMub25SZXNpemUpO1xuICAgICAgICB0aGlzLm9uUmVzaXplKCk7XG5cbiAgICAgICAgJCh3aW5kb3cpLnNjcm9sbFRvcCgwKTtcbiAgICB9LFxuXG4gICAgb25TY3JvbGw6IGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgdmFyIF90ID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpO1xuXG4gICAgICAgIGlmIChfdCA+PSA1NSkge1xuICAgICAgICAgICAgJCgnLnJpZ2h0LXNpZGUnKS5hZGRDbGFzcygnZml4ZWQnKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICQoJy5yaWdodC1zaWRlJykucmVtb3ZlQ2xhc3MoJ2ZpeGVkJyk7XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgb25SZXNpemU6IGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgdmFyICRtYWluVyA9ICQoJy5tYWluLWJsb2NrJykud2lkdGgoKSxcbiAgICAgICAgICAgICR3aW5kb3dXID0gJCh3aW5kb3cpLndpZHRoKCksXG4gICAgICAgICAgICBwZXJjZW50ID0gMzgsXG4gICAgICAgICAgICB3aWR0aCA9ICRtYWluVyAvIDEwMCAqIHBlcmNlbnQ7XG5cbiAgICAgICAgJCgnLnJpZ2h0LXNpZGUnKS5jc3Moe1xuICAgICAgICAgICAgJ3JpZ2h0JyA6ICgkd2luZG93VyAtICRtYWluVykgLyAyLFxuICAgICAgICAgICAgJ3dpZHRoJyA6ICRtYWluVyAvIDEwMCAqIHBlcmNlbnRcbiAgICAgICAgfSk7XG4gICAgfVxufSk7IiwiXG52YXIgdGVtcGxhdGUgPSByZXF1aXJlKCcuLi90ZW1wbGF0ZXMvZm9vdGVyLmh0bWwnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBGb290ZXJWaWV3ID0gTWFyaW9uZXR0ZS5JdGVtVmlldy5leHRlbmQoe1xuICAgIHRlbXBsYXRlOiB0ZW1wbGF0ZVxufSk7IiwiXG52YXIgdGVtcGxhdGUgPSByZXF1aXJlKCcuLi8uLi90ZW1wbGF0ZXMvaG9tZS9mZWF0dXJlZC5odG1sJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gRmVhdHVyZWRWaWV3ID0gTWFyaW9uZXR0ZS5JdGVtVmlldy5leHRlbmQoe1xuICAgIHRlbXBsYXRlOiB0ZW1wbGF0ZSxcblxuICAgIGluaXRpYWxpemU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAvLyAkKHdpbmRvdykub24oJ3Njcm9sbCcsIHRoaXMub25TY3JvbGwpO1xuICAgIH0sXG5cblxuICAgIC8vIG9uUmVuZGVyOiBmdW5jdGlvbigpIHtcbiAgICAvLyAgICAgLy8gdGhpcy5vblNjcm9sbCgpO1xuICAgIC8vIH0sXG5cbiAgICAvLyBvblNjcm9sbDogZnVuY3Rpb24oZSkge1xuICAgIC8vICAgICAvLyB2YXIgX3QgPSAkKHdpbmRvdykuc2Nyb2xsVG9wKCk7XG5cbiAgICAvLyAgICAgLy8gaWYgKF90ID49IDQ0MCkge1xuICAgIC8vICAgICAvLyAgICAgLy8gJCgnLm1haW4tbmF2JykuYWRkQ2xhc3MoJ2ZpeGVkJyk7XG4gICAgLy8gICAgIC8vICAgICAkKCcubWFpbi1uYXYubmF2MScpLmFkZENsYXNzKCdvbicpO1xuICAgIC8vICAgICAvLyAgICAgJCgnLm1haW4tbmF2Lm5hdjInKS5yZW1vdmVDbGFzcygnb24nKTtcbiAgICAvLyAgICAgLy8gfVxuICAgIC8vICAgICAvLyBlbHNlIHtcbiAgICAvLyAgICAgLy8gICAgIC8vICQoJy5tYWluLW5hdicpLnJlbW92ZUNsYXNzKCdmaXhlZCcpO1xuICAgIC8vICAgICAvLyAgICAgJCgnLm1haW4tbmF2Lm5hdjInKS5hZGRDbGFzcygnb24nKTtcbiAgICAvLyAgICAgLy8gICAgICQoJy5tYWluLW5hdi5uYXYxJykucmVtb3ZlQ2xhc3MoJ29uJyk7XG4gICAgLy8gICAgIC8vIH1cbiAgICAvLyB9XG5cbn0pOyIsIlxudmFyIHRlbXBsYXRlID0gcmVxdWlyZSgnLi4vLi4vdGVtcGxhdGVzL2hvbWUvcG9zdHMuaHRtbCcpLFxuICAgIGFydGljbGVUcGwgPSByZXF1aXJlKCcuLi8uLi90ZW1wbGF0ZXMvaG9tZS9hcnRpY2xlLmh0bWwnKSxcbiAgICBkYXRhUHJveHkgPSByZXF1aXJlKCcuLi8uLi9kYXRhUHJveHknKTtcblxudmFyIEFydGljbGUgPSBNYXJpb25ldHRlLkl0ZW1WaWV3LmV4dGVuZCh7XG4gICAgdGFnTmFtZTogJ2FydGljbGUnLFxuICAgIGNsYXNzTmFtZTogJ3Bvc3QgaXRlbTInLFxuICAgIHRlbXBsYXRlOiBhcnRpY2xlVHBsXG59KTtcblxudmFyIEFydGljbGVzID0gTWFyaW9uZXR0ZS5Db2xsZWN0aW9uVmlldy5leHRlbmQoe1xuICAgIGNsYXNzTmFtZTogJ3JvdycsXG4gICAgY2hpbGRWaWV3OiBBcnRpY2xlLFxufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gTWFyaW9uZXR0ZS5MYXlvdXRWaWV3LmV4dGVuZCh7XG4gICAgdGVtcGxhdGU6IHRlbXBsYXRlLFxuXG4gICAgcG9zdE9mZnNldDogMTIsXG4gICAgbG9hZFRoaXNNb3JlIDogNixcblxuICAgIG1vcmVDb2xsZWN0aW9uOiBuZXcgQmFja2JvbmUuQ29sbGVjdGlvbigpLFxuXG4gICAgcmVnaW9uczoge1xuICAgICAgICAnbW9yZVJlZ2lvbicgOiAnLm1vcmUtcG9zdHMnXG4gICAgfSxcblxuICAgIG9uU2hvdzogZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciB0aGF0ID0gdGhpcztcbiAgICAgICAgJCgnLm1haW4tbmF2IGEnKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG5cbiAgICAgICAgdGhpcy5tb3JlQ29sbGVjdGlvbiA9IG5ldyBCYWNrYm9uZS5Db2xsZWN0aW9uKCk7XG4gICAgICAgIHRoaXMubW9yZVZpZXdzID0gbmV3IEFydGljbGVzKHtcbiAgICAgICAgICAgIGNvbGxlY3Rpb246IHRoaXMubW9yZUNvbGxlY3Rpb25cbiAgICAgICAgfSlcblxuICAgICAgICB0aGlzLm1vcmVSZWdpb24uc2hvdyggdGhpcy5tb3JlVmlld3MgKTtcblxuICAgICAgICB0aGlzcG9zdE9mZnNldCA9IDEyO1xuICAgICAgICB0aGlzLnJlc2V0UG9zdHMoKTtcbiAgICB9LFxuXG4gICAgb25EZXN0cm95OiBmdW5jdGlvbigpIHtcbiAgICAgICAgLy8gZGF0YVByb3h5LmdldFBvc3RzKCkuZWFjaChmdW5jdGlvbihtLCBpKSB7XG4gICAgICAgIC8vICAgICBtLnNldCgnbG9hZGVkJywgZmFsc2UpXG4gICAgICAgIC8vIH0pO1xuICAgIH0sXG5cbiAgICBldmVudHM6IHtcbiAgICAgICAgJ2NsaWNrIC5sb2FkLW1vcmUnIDogJ2xvYWRNb3JlJ1xuICAgIH0sXG5cbiAgICBsb2FkTW9yZTogZnVuY3Rpb24oKSB7XG4gICAgICAgIFxuICAgICAgICB2YXIgZGF0YSA9IGRhdGFQcm94eS5nZXRQb3N0cygpLndoZXJlKHsnbG9hZGVkJzpmYWxzZX0pLnNsaWNlKDAsIHRoaXMubG9hZFRoaXNNb3JlKTtcblxuICAgICAgICBpZiAoXy5pc0VtcHR5KGRhdGEpKSB7XG4gICAgICAgICAgICByZXR1cm4gJCgnLmxvYWQtbW9yZScpLmZhZGVPdXQoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMubW9yZUNvbGxlY3Rpb24uYWRkKGRhdGEpO1xuXG4gICAgICAgIHRoaXMucG9zdE9mZnNldCArPSB0aGlzLmxvYWRUaGlzTW9yZTtcbiAgICAgICAgdGhpcy5yZXNldFBvc3RzKCk7XG4gICAgfSxcblxuICAgIHJlc2V0UG9zdHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgdGhhdCA9IHRoaXM7XG4gICAgICAgIGRhdGFQcm94eS5nZXRQb3N0cygpLmVhY2goZnVuY3Rpb24obSwgaSkge1xuICAgICAgICAgICAgaWYgKGkgPCB0aGF0LnBvc3RPZmZzZXQpIHtcbiAgICAgICAgICAgICAgICBtLnNldCgnbG9hZGVkJywgdHJ1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBtLnNldCgnbG9hZGVkJywgZmFsc2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG59KTsiLCJcbnZhciB0ZW1wbGF0ZSA9IHJlcXVpcmUoJy4uL3RlbXBsYXRlcy9tYWluLWxheW91dC5odG1sJyksXG4gICAgZGF0YVByb3h5ID0gcmVxdWlyZSgnLi4vZGF0YVByb3h5JyksXG5cbiAgICAvLyBWaWV3c1xuICAgIFRvcG5hdlZpZXcgICAgICAgPSByZXF1aXJlKCcuL3RvcG5hdlZpZXcnKSxcbiAgICBTaWRlbmF2VmlldyAgICAgID0gcmVxdWlyZSgnLi9zaWRlbmF2VmlldycpLFxuICAgIEZvb3RlclZpZXcgICAgICAgPSByZXF1aXJlKCcuL2Zvb3RlclZpZXcnKSxcblxuICAgIEhvbWVmZWF0dXJlZFZpZXcgPSByZXF1aXJlKCcuL2hvbWUvZmVhdHVyZWRWaWV3JyksXG5cbiAgICBIb21lTGF5b3V0ICAgICAgID0gcmVxdWlyZSgnLi9ob21lL2hvbWVMYXlvdXQnKSxcblxuICAgIFBvc3RWaWV3ICAgICAgICAgPSByZXF1aXJlKCcuL3Bvc3RWaWV3JyksXG5cbiAgICBDYXRlZ29yeUxheW91dCAgID0gcmVxdWlyZSgnLi9jYXRlZ29yeS9jYXRlZ29yeUxheW91dCcpO1xuXG5cbm1vZHVsZS5leHBvcnRzID0gTWFpbkxheW91dCA9IE1hcmlvbmV0dGUuTGF5b3V0Vmlldy5leHRlbmQoe1xuXG4gICAgdGVtcGxhdGU6IHRlbXBsYXRlLFxuXG4gICAgdmlld3M6IHt9LFxuXG4gICAgcmVnaW9uczoge1xuICAgICAgICB0b3BuYXZSZWdpb24gICAgICA6ICcudG9wbmF2JyxcbiAgICAgICAgc2lkZW5hdlJlZ2lvbiAgICAgOiAnLnNpZGUtbWVudScsXG4gICAgICAgIGZvb3RlclJlZ2lvbiAgICAgIDogJy5tYWluLWZvb3RlcicsXG5cbiAgICAgICAgZmVhdHVyZWRSZWdpb24gICAgOiAnLmhvbWUtY29udGVudCcsXG4gICAgICAgIG1haW5Db250ZW50UmVnaW9uIDogJy5tYWluLWNvbnRlbnQnXG4gICAgfSxcblxuICAgIGV2ZW50czoge1xuICAgICAgICAnY2xpY2sgLm1lbnUtaWNvbiwgLm1lbnUtb3ZlcmxheScgOiAndG9nZ2xlTWVudSdcbiAgICB9LFxuXG4gICAgb25SZW5kZXI6IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIC8vIHRvcG5hdlxuICAgICAgICB2YXIgbWVudU1vZGVsID0gbmV3IEJhY2tib25lLk1vZGVsKHtcbiAgICAgICAgICAgICdtZW51JzogZGF0YVByb3h5LmdldENhdGVnb3JpZXMoeydtYWluLW1lbnUnOidjaGVja2VkJ30pLnRvSlNPTigpXG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLnZpZXdzLnRvcG5hdiA9IG5ldyBUb3BuYXZWaWV3KHttb2RlbDogbWVudU1vZGVsfSk7XG4gICAgICAgIHRoaXMudG9wbmF2UmVnaW9uLnNob3coIHRoaXMudmlld3MudG9wbmF2ICk7XG5cbiAgICAgICAgLy8gc2lkZW5hdlxuICAgICAgICB2YXIgc2lkZW5hdk1vZGVsID0gbmV3IEJhY2tib25lLk1vZGVsKHtcbiAgICAgICAgICAgICdjYXRlZ29yaWVzJzogZGF0YVByb3h5LmdldENhdGVnb3JpZXMoKS50b0pTT04oKSxcbiAgICAgICAgICAgICd3b3JkaWVzJzogZGF0YVByb3h5LmdldFdvcmRpZXMoKS50b0pTT04oKVxuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy52aWV3cy5zaWRlbmF2ID0gbmV3IFNpZGVuYXZWaWV3KHttb2RlbDogc2lkZW5hdk1vZGVsfSk7XG4gICAgICAgIHRoaXMuc2lkZW5hdlJlZ2lvbi5zaG93KCB0aGlzLnZpZXdzLnNpZGVuYXYgKTtcblxuICAgICAgICB0aGlzLnZpZXdzLmZvb3RlciA9IG5ldyBGb290ZXJWaWV3KCk7XG4gICAgICAgIHRoaXMuZm9vdGVyUmVnaW9uLnNob3coIHRoaXMudmlld3MuZm9vdGVyICk7XG5cbiAgICB9LFxuXG5cbiAgICBob21lOiBmdW5jdGlvbigpIHtcblxuICAgICAgICAvLyBmZWF0dXJlZFxuICAgICAgICB2YXIgZmVhdHVyZWQgPSBkYXRhUHJveHkuZ2V0UG9zdHMoKS5maWx0ZXIoZnVuY3Rpb24obW9kZWwpIHtcbiAgICAgICAgICAgIHZhciBmID0gbW9kZWwuZ2V0KCdmZWF0dXJlZF9pbWFnZScpO1xuICAgICAgICAgICAgcmV0dXJuICgodHlwZW9mKGYpICE9PSAndW5kZWZpbmVkJykgJiYgKGYhPT1udWxsKSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHZhciBmZWF0dXJlZE1vZGVsID0gbmV3IEJhY2tib25lLk1vZGVsKHtcbiAgICAgICAgICAgICdjYXRlZ29yaWVzJzogZGF0YVByb3h5LmdldENhdGVnb3JpZXMoKS50b0pTT04oKSxcbiAgICAgICAgICAgICdmZWF0dXJlZCcgOiBmZWF0dXJlZC5zbGljZSgwLCAzKVxuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy52aWV3cy5ob21lZmVhdHVyZWRWaWV3ID0gbmV3IEhvbWVmZWF0dXJlZFZpZXcoe21vZGVsOiBmZWF0dXJlZE1vZGVsfSk7XG4gICAgICAgIHRoaXMuZmVhdHVyZWRSZWdpb24uc2hvdyggdGhpcy52aWV3cy5ob21lZmVhdHVyZWRWaWV3ICk7XG5cblxuICAgICAgICAvLyBob21lXG4gICAgICAgIHRoaXMudmlld3MuaG9tZUxheW91dCA9IG5ldyBIb21lTGF5b3V0KHtcbiAgICAgICAgICAgIGNvbGxlY3Rpb246IGRhdGFQcm94eS5nZXRQb3N0cygpXG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLm1haW5Db250ZW50UmVnaW9uLnNob3coIHRoaXMudmlld3MuaG9tZUxheW91dCApO1xuICAgIH0sXG5cblxuICAgIHBvc3Q6IGZ1bmN0aW9uKGlkKSB7XG4gICAgICAgIHZhciB0aGF0ID0gdGhpcztcbiAgICAgICAgaWYgKHRoaXMudmlld3MuaG9tZWZlYXR1cmVkVmlldykge1xuICAgICAgICAgICAgdGhpcy52aWV3cy5ob21lZmVhdHVyZWRWaWV3LmRlc3Ryb3koKTtcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgZGF0YVByb3h5LmdldFBvc3RCeUlkKGlkLCBmdW5jdGlvbihtKSB7XG4gICAgICAgICAgICB2YXIgbW9kZWwgPSBtO1xuICAgICAgICAgICAgdmFyIGNhdGVnb3J5X2lkID0gbW9kZWwuZ2V0KCd0ZXJtcycpLmNhdGVnb3J5WzBdLnNsdWc7XG4gICAgICAgICAgICB2YXIgcmVsYXRlZCA9IGRhdGFQcm94eS5nZXRQb3N0c0J5Q2F0ZWdvcnkoY2F0ZWdvcnlfaWQpLnRvSlNPTigpO1xuICAgICAgICAgICAgbW9kZWwuc2V0KCdyZWxhdGVkJywgcmVsYXRlZCk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoYXQudmlld3MucG9zdCA9IG5ldyBQb3N0Vmlldyh7XG4gICAgICAgICAgICAgICAgbW9kZWw6IG1vZGVsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHRoYXQubWFpbkNvbnRlbnRSZWdpb24uc2hvdyggdGhhdC52aWV3cy5wb3N0ICk7XG4gICAgICAgIH0pO1xuICAgIH0sXG5cblxuICAgIGNhdGVnb3J5OiBmdW5jdGlvbihpZCkge1xuICAgICAgICB2YXIgdGhhdCA9IHRoaXM7XG4gICAgICAgIGlmICh0aGlzLnZpZXdzLmhvbWVmZWF0dXJlZFZpZXcpIHtcbiAgICAgICAgICAgIHRoaXMudmlld3MuaG9tZWZlYXR1cmVkVmlldy5kZXN0cm95KCk7XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgbW9kZWwgPSBkYXRhUHJveHkuZ2V0Q2F0ZWdvcmllcygpLmZpbmRXaGVyZSh7J3NsdWcnOiBpZH0pO1xuICAgICAgICBtb2RlbC5zZXQoJ2l0ZW1zJywgZGF0YVByb3h5LmdldFBvc3RzQnlDYXRlZ29yeShpZCkudG9KU09OKCkpO1xuICAgICAgICBtb2RlbC5zZXQoJ3dvcmRpZXMnLCBkYXRhUHJveHkuZ2V0V29yZGllcygpLnRvSlNPTigpKTtcblxuICAgICAgICAvLyBjYXRlZ29yeVxuICAgICAgICB0aGlzLnZpZXdzLmNhdGVnb3J5TGF5b3V0ID0gbmV3IENhdGVnb3J5TGF5b3V0KHtcbiAgICAgICAgICAgIG1vZGVsOiBtb2RlbFxuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5tYWluQ29udGVudFJlZ2lvbi5zaG93KCB0aGlzLnZpZXdzLmNhdGVnb3J5TGF5b3V0ICk7XG4gICAgfSxcblxuXG4gICAgd29yZGllczogZnVuY3Rpb24oaWQpIHtcbiAgICAgICAgdmFyIHRoYXQgPSB0aGlzO1xuICAgICAgICBpZiAodGhpcy52aWV3cy5ob21lZmVhdHVyZWRWaWV3KSB7XG4gICAgICAgICAgICB0aGlzLnZpZXdzLmhvbWVmZWF0dXJlZFZpZXcuZGVzdHJveSgpO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIG1vZGVsID0gZGF0YVByb3h5LmdldFdvcmRpZXMoKS5maW5kV2hlcmUoeydzbHVnJzogaWR9KTtcbiAgICAgICAgbW9kZWwuc2V0KCdpdGVtcycsIGRhdGFQcm94eS5nZXRQb3N0c0J5V29yZGllcyhpZCkudG9KU09OKCkpO1xuICAgICAgICBtb2RlbC5zZXQoJ3dvcmRpZXMnLCBkYXRhUHJveHkuZ2V0V29yZGllcygpLnRvSlNPTigpKTtcblxuICAgICAgICAvLyBjYXRlZ29yeVxuICAgICAgICB0aGlzLnZpZXdzLmNhdGVnb3J5TGF5b3V0ID0gbmV3IENhdGVnb3J5TGF5b3V0KHtcbiAgICAgICAgICAgIG1vZGVsOiBtb2RlbFxuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5tYWluQ29udGVudFJlZ2lvbi5zaG93KCB0aGlzLnZpZXdzLmNhdGVnb3J5TGF5b3V0ICk7XG4gICAgfSxcblxuXG4gICAgdG9nZ2xlTWVudTogZnVuY3Rpb24oZSkge1xuICAgICAgICBpZiAoJCgnLm1lbnUtYW5pbScpLmhhc0NsYXNzKCdhbmltYXRlJykpXG4gICAgICAgIHtcbiAgICAgICAgICAgICQoJy5tZW51LWFuaW0nKS5yZW1vdmVDbGFzcygnYW5pbWF0ZScpO1xuICAgICAgICAgICAgJCgnYm9keScpLnJlbW92ZUNsYXNzKCdtZW51LW9uJyk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAkKCcubWVudS1hbmltJykuYWRkQ2xhc3MoJ2FuaW1hdGUnKTtcbiAgICAgICAgICAgICQoJ2JvZHknKS5hZGRDbGFzcygnbWVudS1vbicpO1xuICAgICAgICB9XG4gICAgfVxuXG5cbn0pOyIsIlxudmFyIHRlbXBsYXRlID0gcmVxdWlyZSgnLi4vdGVtcGxhdGVzL3Bvc3QuaHRtbCcpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IFBvc3RWaWV3ID0gTWFyaW9uZXR0ZS5JdGVtVmlldy5leHRlbmQoe1xuICAgIHRlbXBsYXRlOiB0ZW1wbGF0ZSxcblxuICAgIG9uU2hvdzogZnVuY3Rpb24oKSB7XG4gICAgICAgICQod2luZG93KS5zY3JvbGxUb3AoMCk7XG4gICAgfVxuXG59KTsiLCJcbnZhciB0ZW1wbGF0ZSA9IHJlcXVpcmUoJy4uL3RlbXBsYXRlcy9zaWRlbmF2Lmh0bWwnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBTaWRlbmF2VmlldyA9IE1hcmlvbmV0dGUuSXRlbVZpZXcuZXh0ZW5kKHtcbiAgICB0ZW1wbGF0ZTogdGVtcGxhdGUsXG5cbiAgICBldmVudHM6IHtcbiAgICAgICAgJ2NsaWNrIGxpIGEnIDogJ2Nsb3NlTWVudSdcbiAgICB9LFxuXG4gICAgY2xvc2VNZW51OiBmdW5jdGlvbigpIHtcbiAgICAgICAgJCgnLm1lbnUtaWNvbicpLmNsaWNrKCk7XG4gICAgfVxufSk7IiwiXG52YXIgdGVtcGxhdGUgPSByZXF1aXJlKCcuLi90ZW1wbGF0ZXMvdG9wbmF2Lmh0bWwnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBUb3BuYXZWaWV3ID0gTWFyaW9uZXR0ZS5JdGVtVmlldy5leHRlbmQoe1xuICAgIHRlbXBsYXRlOiB0ZW1wbGF0ZSxcblxuICAgIGV2ZW50czoge1xuICAgICAgICAnY2xpY2sgLnRleHQtbG9nbycgOiAnZ29Ib21lJyxcbiAgICAgICAgJ2NsaWNrIC5tYWluLW5hdiBhJyA6ICdnb3RvQ2F0ZWdvcnknXG4gICAgfSxcblxuICAgIGdvSG9tZTogZnVuY3Rpb24oZSkge1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIEJhY2tib25lLmhpc3RvcnkubmF2aWdhdGUoJycsIHRydWUpO1xuICAgIH0sXG5cblxuICAgIGdvdG9DYXRlZ29yeTogZnVuY3Rpb24oZSkge1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHZhciAkdGFyZ2V0ID0gJChlLmN1cnJlbnRUYXJnZXQpO1xuICAgICAgICB2YXIgaHJlZiA9ICR0YXJnZXQuYXR0cignaHJlZicpO1xuICAgICAgICBCYWNrYm9uZS5oaXN0b3J5Lm5hdmlnYXRlKCdjYXRlZ29yeS8nK2hyZWYsIHRydWUpO1xuXG4gICAgICAgICQoJy5tYWluLW5hdiBhJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAkdGFyZ2V0LmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICB9XG59KTsiXX0=
