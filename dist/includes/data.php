<?php

$cats = array('comics','films','tv','video-games','cosplay','gadgets','screenings','features');
$sections = array(
	'comics' => array('name'=>'Comics', 'slug'=>'comics'),
	'films' => array('name'=>'Films', 'slug'=>'films'),
	'tv' => array('name'=>'TV', 'slug'=>'tv'),
	'video-games' => array('name'=>'Video Games', 'slug'=>'video-games'),
	'cosplay' => array('name'=>'Cosplay', 'slug'=>'cosplay'),
	'anime' => array('name'=>'Anime', 'slug'=>'anime'),
	'gadgets' => array('name'=>'Gadgets', 'slug'=>'gadgets'),
	'screenings' => array('name'=>'Screenings', 'slug'=>'screenings'),
	'features' => array('name'=>'Features', 'slug'=>'features'),
);

$moreMenu = array_slice($sections, 4);

$xml_string = file_get_contents('http://www.polygon.com/rss/group/news/index.xml');
$xml = simplexml_load_string($xml_string);
$json = json_encode($xml);
$data = json_decode($json);

$entry = $data->entry;

$detail_posts = array();

foreach ($entry as &$e) {
	preg_match('/<img.+src=[\'"](?P<src>.+)[\'"].*>/i', $e->content, $image);
	$e->image = $image['src'];
	$e->excerpt = substr(strip_tags($e->content), 0, 300) . '...';
	$e->id = preg_replace('/http\:\/\/www\.polygon\.com\//', '', $e->id);

	$e->cat = $sections[$cats[rand(0, 8)]];
	
	$detail_posts[$e->id] = $e;
}

$posts = array(
	'featured' => array_slice($entry, 0, -7),
	'row1' => array($entry[3]),
	'row2' => array_slice($entry, 4, 3),
	'row3' => array_slice($entry, 7, 2)
);


if ($page_id) {
	foreach ($entry as $e) {
		if ($e->id == $page_id) {
			$post = $e;
		}
	}
}


// header('Content-Type: application/json');
// echo json_encode($entry);
// exit();