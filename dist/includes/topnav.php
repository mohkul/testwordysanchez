		<div class="topnav">
			<div class="menu-icon menu-anim"><span></span></div>

			<a href="/" class="text-logo">WORDY SANCHEZ</a>

			<div class="main-nav nav1 menu-anim <?php echo ($section) ? 'on':''?>">
				<nav>
					<ul>
						<?php foreach ($sections as $s) {
							$active = ($section==$s['slug']) ? 'active' : '';
							?>
							<li class="<?php echo $s['slug']?>">
								<a href="/cat/<?php echo $s['slug']?>" class="<?php echo $s['slug']?> <?php echo $active?>"><?php echo $s['name']?></a>
							</li>
						<?php } ?>
						
						<li class="more"><a href="more" class="more">MORE</a>
		
							<div class="dd-menu">
								<ul>
									<?php foreach (array_slice($sections, 4) as $s) {
										$active = ($section==$s['slug']) ? 'active' : '';
										?>
										<li>
											<a href="/cat/<?php echo $s['slug']?>" class="<?php echo $s['slug']?> <?php echo $active?>"><?php echo $s['name']?></a>
										</li>
									<?php } ?>
								</ul>
							</div>
						</li>
					</ul>
				</nav>
			</div>

			<div class="top-right">
				<div class="social">
					<div class="icon icon-facebook"></div>
					<div class="icon icon-twitter"></div>
					<div class="icon icon-instagram"></div>
					<div class="icon icon-googleplus"></div>
				</div>

				<div class="search-box">
					<input type="text" id="search" placeholder="SEARCH">
					<span class="icon-search"></span>
				</div>
			</div>

		</div>

		<div class="side-menu menu-anim">
			<div class="main">
				<ul>
					<li><a href="/" class="on">HOME PAGE</a></li>
					<?php foreach ($sections as $s) {
						$active = ($section==$s['slug']) ? 'active' : '';
						?>
						<li class="<?php echo $s['slug']?>">
							<a href="/cat/<?php echo $s['slug']?>" class="<?php echo $s['slug']?> <?php echo $active?>"><?php echo $s['name']?></a>
						</li>
					<?php } ?>
				</ul>
			</div>

			<div class="wordies">
				<h3>WORDIES</h3>
				<ul>
					<li><a href="#">Build It Your Way</a></li>
					<li><a href="#">You May Be Cousins</a></li>
					<li><a href="#">Crate Digging</a></li>
					<li><a href="#">They Shoot Chupacabras Don't They</a></li>
					<li><a href="#">Fight Night</a></li>
					<li><a href="#">Otra Vez</a></li>
					<li><a href="#">The Pros of Cons</a></li>
					<li><a href="#">Ghuy's Basement</a></li>
					<li><a href="#">Match Con</a></li>
					<li><a href="#">The Great Defender</a></li>
					<li><a href="#">Kung-Fu Grip</a></li>
				</ul>
			</div>

		</div>
		<div class="menu-overlay"></div>