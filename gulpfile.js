var gulp = require('gulp');
var util = require('gulp-util');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var browserify = require('gulp-browserify');
var minifyHTML = require('gulp-minify-html');
var stylus = require('gulp-stylus');
var imagemin = require('gulp-imagemin');
var connect = require('gulp-connect');
var rename = require('gulp-rename');
var underscorify = require('node-underscorify');

var paths = {
    src: './src',
    dist: './dist'
};

var files = {
    scripts: [paths.src+'/scripts/**/*.js'],
    html: [paths.src+'/**/*.html'],
    stylus: [paths.src+'/styles/**/*.styl'],
    images: [
        paths.src+'/images/**/*.png',
        paths.src+'/images/**/*.jpg',
        paths.src+'/images/**/*.gif'
    ]
};

var tplTransform = require('node-underscorify').transform({
    extensions: ['ejs', 'html']
});

gulp.task('scripts', function (cb) {
    return gulp.src(paths.src + '/scripts/main.js' )
        .pipe(browserify({
            debug: true,
            transform: [tplTransform]
        }))
        .pipe(rename('build.js'))
        .pipe(gulp.dest(paths.dist+'/js'))
        .pipe(connect.reload());
});

gulp.task('html', function() {
    var opts = {comments:true, spare:true};
    gulp.src(files.html)
        .pipe(minifyHTML(opts))
        .pipe(gulp.dest(paths.dist))
        .pipe(connect.reload());
});

gulp.task('stylus', function() {
    gulp.src(files.stylus)
        .pipe(stylus({use: ['nib']}))
        .pipe(gulp.dest(paths.dist+'/css'))
        .pipe(connect.reload());
});

gulp.task('images', function() {
    gulp.src(files.images)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.dist+'/images'))
        .pipe(connect.reload());
});

gulp.task('connect', connect.server({
    root: [paths.dist],
    port: 1338,
    livereload: true
}));

gulp.task('watch', function () {
    gulp.watch(files.scripts, ['scripts']);
    gulp.watch(files.html, ['html']);
    gulp.watch(files.stylus, ['stylus']);
    gulp.watch(files.images, ['images']);
});




gulp.task('concat:vendor', function() {
    gulp.src([
        'bower_components/jquery/jquery.js',
        'bower_components/underscore/underscore.js',
        'bower_components/backbone/backbone.js',
        'bower_components/marionette/lib/backbone.marionette.js',
        'bower_components/scrollToTop/dist/jquery.scrolltotop.js'
        ])
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(paths.dist+'/js'));
});


gulp.task('build', [
    "concat:vendor"
]);


gulp.task('default', [
    'scripts',
    'html',
    'stylus',
    'images',
    'connect',
    'watch'
]);

