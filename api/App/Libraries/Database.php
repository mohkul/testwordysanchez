<?php

namespace App\Libraries;

use \PDO;

require_once 'App/Config/Database.php';

class Database extends PDO {
	
	protected static $instance;

	public function __construct() {}

	public static function getInstance() {

		if (!self::$instance) {

			self::$instance = new PDO("mysql:host=".DBHOST.';port='.DBPORT.';dbname='.DBNAME, DBUSER, DBPASSWORD);

		}

		return self::$instance;
	}

	public static function fetchAll($query) {
		$stm = self::$instance->prepare($query);
		try {
			$stm->execute();
		}
		catch (PDOException $Exception) {
			throw new MyDatabaseException( $Exception->getMessage( ) , $Exception->getCode( ) );
		}

		return $stm->fetchAll(\PDO::FETCH_ASSOC);
	}


	public static function fetchOne($query) {
		$stm = self::$instance->prepare($query);
		try {
			$stm->execute();
		}
		catch (PDOException $Exception) {
			throw new MyDatabaseException( $Exception->getMessage( ) , $Exception->getCode( ) );
		}

		return $stm->fetch(\PDO::FETCH_ASSOC);
	}

}