<?php

namespace App\Models;

require_once 'App/Libraries/Database.php';
use App\Libraries\Database;

class AppModel {

	private $db;

	function __construct() {
		$this->db = Database::getInstance();
	}

	// public function getCategories() {
	// 	$query = "SELECT `wp_terms`.`term_id` AS `id`, `name`, `slug`
	// 		FROM `wp_terms`
	// 		LEFT JOIN `wp_term_taxonomy`
	// 			ON `wp_term_taxonomy`.`term_id` = `wp_terms`.`term_id`
	// 		WHERE `wp_term_taxonomy`.`taxonomy` = 'category'";

	// 	return Database::fetchAll($query);
	// }


	public function getCategories() {
		$query = "SELECT `tt`.`taxonomy`,
						 `t`.`name`,
						 `t`.`slug`,
						 `tt`.`count`,
						 `tm`.`meta_key`,
						 `tm`.`meta_value`
					FROM `wp_term_taxonomy` AS `tt`
					LEFT JOIN `wp_terms` AS `t`
						ON (`t`.`term_id` = `tt`.`term_id`)
					LEFT JOIN `wp_termsmeta` AS `tm`
						ON (`tm`.`terms_id` = `tt`.`term_id`)
					WHERE `tt`.`taxonomy` = 'category'
					ORDER BY `t`.`term_order`";

		return Database::fetchAll($query);
	}


	public function getWordies() {
		$query = "SELECT `tt`.`taxonomy`,
						 `t`.`name`,
						 `t`.`slug`,
						 `tt`.`count`,
						 `tm`.`meta_key`,
						 `tm`.`meta_value`
					FROM `wp_term_taxonomy` AS `tt`
					LEFT JOIN `wp_terms` AS `t`
						ON (`t`.`term_id` = `tt`.`term_id`)
					LEFT JOIN `wp_termsmeta` AS `tm`
						ON (`tm`.`terms_id` = `tt`.`term_id`)
					WHERE `tt`.`taxonomy` = 'wordy'
					ORDER BY `t`.`term_order`";

		return Database::fetchAll($query);
	}


};
