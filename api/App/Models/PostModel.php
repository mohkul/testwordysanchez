<?php

namespace App\Models;

require_once 'App/Libraries/Database.php';
use App\Libraries\Database;

class PostModel {

	private $db;

	function __construct() {
		$this->db = Database::getInstance();
	}

	public function getPosts() {
		$query = "SELECT `p`.`ID`,
						 `p`.`post_name`    AS `slug`,
						 `p`.`post_title`   AS `title`,
						 `p`.`post_author`  AS `author`,
						 `p`.`post_content` AS `content`,
						 `p`.`post_excerpt` AS `excerpt`,
						 `p`.`post_date`    AS `date`,
						 `p`.`post_author`  AS `author`,

						 -- `u`.`display_name`,

						 `pm`.`meta_key`,
						 `pm`.`meta_value`
					FROM `wp_posts` AS `p`

					-- LEFT JOIN `wp_users` AS `u`
					-- 	ON (`u`.`ID` = `p`.`post_author`)

					LEFT JOIN `wp_postmeta` AS `pm`
						ON (`pm`.`post_id` = `p`.`ID`)

				   WHERE (`p`.`post_type` = 'post'
			  		 AND `p`.`post_status` = 'publish')

					 AND `pm`.`meta_key` = '_thumbnail_id'

				ORDER BY `p`.`post_date` DESC";

		return Database::fetchAll($query);
	}

	public function getPostAuthor($id) {
		$query = "SELECT ID, display_name
					FROM wp_users
					WHERE ID = $id
		";

		return Database::fetchOne($query);
	}

	public function getPostThumbnail($id) {
		$query = "SELECT `meta_value` AS 'source'
					FROM `wp_postmeta`
					WHERE `post_id` = $id
					  AND `meta_key` = '_wp_attached_file'";

		return Database::fetchOne($query);
	}

	public function getPostTerms($id) {
		$query = "SELECT t.term_id, taxonomy, name, slug, count FROM wp_term_relationships AS ts
				LEFT JOIN wp_term_taxonomy AS tt ON (tt.term_taxonomy_id = ts.term_taxonomy_id)
				LEFT JOIN wp_terms AS t ON (t.term_id = tt.term_id)
				  WHERE object_id = $id";

		return Database::fetchAll($query);
	}


	public function saveEntry($p) {
		// $statement = $this->db->prepare($query);
		// $statement->bindParam(":firstname", $p["firstname"], PDO::PARAM_STR);

		// try {

		// 	$statement->execute();
		// }
		// catch ( PDOException $Exception ) {

		// 	throw new MyDatabaseException( $Exception->getMessage( ) , $Exception->getCode( ) );

		// }


		// return $statement->rowCount();
	}

};