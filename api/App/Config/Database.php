<?php

namespace App\Config;

define("DOMAIN", $_SERVER["HTTP_HOST"]);

switch (DOMAIN) {

	// Development
	case "localhost" :
	case "wordy.local":
		define("DBHOST",		"localhost");
		define("DBUSER",		"root");
		define("DBPASSWORD",	"lockdown");
		define("DBPORT",        3306);
		define("DBNAME",	    "wordpress");
		break;

	// Staging
	case "wordy.homebrew.agency" :
		define("DBHOST",		"localhost");
		define("DBUSER",		"root");
		define("DBPASSWORD",	"homebrew138");
		define("DBPORT",        3306);
		define("DBNAME",	    "wordpress");
		break;


	// QA
	case "my-qa-server.com" :
		define("DBHOST",		"");
		define("DBUSER",		"");
		define("DBPASSWORD",	"");
		define("DBPORT",        3306);
		define("DBNAME",	    "");
		break;

	// Production
	case "my-production-server.com" :
		define("DBHOST",		"");
		define("DBUSER",		"");
		define("DBPASSWORD",	"");
		define("DBPORT",        3306);
		define("DBNAME",	    "");
		break;

	default :
		exit('The application environment is not set correctly.');

}
