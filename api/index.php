<?php

require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

require_once 'App/Models/AppModel.php';
require_once 'App/Models/PostModel.php';
use App\Models\AppModel;
use App\Models\PostModel;

$app = new \Slim\Slim();

$app->get('/posts', 'getPosts');
$app->get('/term/:id', 'getTerm');
$app->get('/site', 'getSite');

function printJSON($a) {
    header('Content-Type: application/json');
    echo json_encode($a);
}

function getPosts() {
	$posts = new PostModel();
	$p = $posts->getPosts();

	foreach($p as &$post) {
		foreach($post as $key => &$val) {			
			if ($key == 'content') {
				$val = iconv('UTF-8', 'UTF-8//IGNORE', utf8_encode($val));
                $val = preg_replace( "/\r|\n/", " ", $val );
			}
		}

        $content = preg_replace("/\[caption([^\[\]]*)\]([^\[\]]*)\[\/caption\]/i", "", strip_tags($post['content']));
        $words = explode(' ', $content);
        $excerpt = array_slice($words, 0, 55);
        $post['excerpt'] = implode(' ', $excerpt);

        $post['content'] = 'test';

        // get link
        $post['link'] = 'posts/'.$post['ID'].'/'.$post['slug'];
        
        // get author
        $post['author'] = $posts->getPostAuthor($post['author']);

        // get featured image
        if ($post['meta_value']) {
            $post['featured_image'] = $posts->getPostThumbnail($post['meta_value']);
            $post['featured_image']['source'] = '/wp/wp-content/uploads/' . $post['featured_image']['source'];
        }

        // get terms
        $terms = $posts->getPostTerms($post['ID']);
        $post['terms'] = array(
            'wordy' => array(),
            'category' => array(),
            'post_tag' => array()
        );
        foreach ($terms as $t) {
            $push = array(
                'ID'   => $t['term_id'],
                'name' => $t['name'],
                'slug' => $t['slug'],
                'count' => $t['count']
            );

            array_push($post['terms'][$t['taxonomy']], $push);
        }

	}

    return printJSON($p);
}

function getTerm($id) {
    $appModel = new AppModel();
    if ($id=='category') {
        $terms = $appModel->getCategories();
    }
    else if ($id=='wordy') {
        $terms = $appModel->getWordies();   
    }

    $return = array();

    function get_slugs($c) {
        return $c['slug'];
    }

    foreach($terms as $cat) {
        $slugs = array_map("get_slugs", $return);        

        if (!in_array($cat['slug'], $slugs)) {
            $item = array(
                'name' => $cat['name'],
                'slug' => $cat['slug'],
                'count' => $cat['count']
            );

            if ($cat['meta_key'] != null) {
                $item[$cat['meta_key']] = $cat['meta_value'];
            }

            array_push($return, $item);
        }
        else {
            if ($cat['meta_key'] != null) {

                foreach($return as &$r) {
                    if ($r['slug'] == $cat['slug']) {
                        $r[$cat['meta_key']] = $cat['meta_value'];
                    }
                }
            }            
        }
    }

    return printJSON($return);
}


function getSite() {
    echo 'site';
//     $appModel = new AppModel();
//     $categories = $appModel->getCategories();
//     $wordies = $appModel->getWordies();

//     $site = array(
//         'global' => array(
//             'categories' => $categories,
//             'wordies' => $wordies
//         )
//     );

//     return printJSON($site);
}

$app->run();
